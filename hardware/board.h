/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio
    Modified by K

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#ifndef _BOARD_H_
#define _BOARD_H_

/*
 * Setup for the radio iface board.
 */

/*
 * Board identifier.
 */
#define BOARD_RADIO_IF
#define BOARD_NAME              "Radio IF"

/*
 * Board frequencies.
 */
#define STM32_LSECLK            32768
#define STM32_HSECLK            8000000

/*
 * MCU type, supported types are defined in ./os/hal/platforms/hal_lld.h.
 */
#define STM32F10X_MD

/*
 * IO pins assignments.
 */
#define GPIOA_SPI1NSS           4
#define GPIOB_SPI2NSS           12

#define GPIOB_LED_DATA_RX       5
#define GPIOB_LED_DATA_TX       6
#define GPIOB_LED_STATUS        7

#define GPIOC_USB_DISC          13

/*
 * I/O ports initial setup, this configuration is established soon after reset
 * in the initialization code.
 *
 * The digits have the following meaning:
 *   0 - Analog input.
 *   1 - Push Pull output 10MHz.
 *   2 - Push Pull output 2MHz.
 *   3 - Push Pull output 50MHz.
 *   4 - Digital input.
 *   5 - Open Drain output 10MHz.
 *   6 - Open Drain output 2MHz.
 *   7 - Open Drain output 50MHz.
 *   8 - Digital input with PullUp or PullDown resistor depending on ODR.
 *   9 - Alternate Push Pull output 10MHz.
 *   A - Alternate Push Pull output 2MHz.
 *   B - Alternate Push Pull output 50MHz.
 *   C - Reserved.
 *   D - Alternate Open Drain output 10MHz.
 *   E - Alternate Open Drain output 2MHz.
 *   F - Alternate Open Drain output 50MHz.
 * Please refer to the STM32 Reference Manual for details.
 */

/*
 * Port A setup.
 * Everything input with pull-up except:
 * PA2  - Push Pull output 10MHz (RF22 SDN).
 * PA3  - Digital input with pullup (RF22 IRQ).
 * PA4  - Push Pull output 10MHz (RF22 CS).
 * PA5  - Alternate Push Pull output 10MHz (RF22 SCK).
 * PA6  - Digital input (RF22 SDO).
 * PA7  - Alternate Push Pull output 10MHz (RF22 SDI).
 * PA11 - Digital input (USB DM).
 * PA12 - Digital input (USB DP).
 */
#define VAL_GPIOACRL            0x94918188      /*  PA7...PA0 */
#define VAL_GPIOACRH            0x88844888      /* PA15...PA8 */
#define VAL_GPIOAODR            0xFFFFFFFF

/*
 * Port B setup.
 * Everything input with pull-up except:
 * PB0  - Digital input with pullup (RF22 IO1).
 * PB1  - Digital input with pullup (RF22 IO2).
 * PB5  - Open Drain output 2MHz (DATA RX).
 * PB6  - Open Drain output 2MHz (DATA TX).
 * PB7  - Open Drain output 2MHz (STATUS).
 * PB12 - Push Pull output 10MHz (RF24 CS).
 * PB13 - Alternate Push Pull output 10MHz (RF24 SCK).
 * PB14 - Digital input (RF24 MISO).
 * PB15 - Alternate Push Pull output 10MHz (RF24 MOSI).
 */
#define VAL_GPIOBCRL            0x66688888      /*  PB7...PB0 */
#define VAL_GPIOBCRH            0x94918888      /* PB15...PB8 */
#define VAL_GPIOBODR            0xFFFFFFFF

/*
 * Port C setup.
 * Everything input with pull-up except:
 * PC5  - Digital input with pullup (RF22 IO3).
 * PC10 - Push Pull output 10MHz (RF24 CE).
 * PC11 - Digital input with pullup (RF24 IRQ).
 * PC13 - Push Pull output 2MHz (USB DISC).

 */
#define VAL_GPIOCCRL            0x88888888      /*  PC7...PC0 */
#define VAL_GPIOCCRH            0x88288188      /* PC15...PC8 */
#define VAL_GPIOCODR            0xFFFFFFFF

/*
 * Port D setup.
 * Everything input with pull-up except:
 * PD0  - Normal input (XTAL).
 * PD1  - Normal input (XTAL).
 */
#define VAL_GPIODCRL            0x88888844      /*  PD7...PD0 */
#define VAL_GPIODCRH            0x88888888      /* PD15...PD8 */
#define VAL_GPIODODR            0xFFFFFFFF

/*
 * Port E setup.
 * Everything input with pull-up except:
 */
#define VAL_GPIOECRL            0x88888888      /*  PE7...PE0 */
#define VAL_GPIOECRH            0x88888888      /* PE15...PE8 */
#define VAL_GPIOEODR            0xFFFFFFFF

/*
 * USB bus activation macro, required by the USB driver.
 */
#define usb_lld_connect_bus(usbp) palClearPad(GPIOC, GPIOC_USB_DISC)

/*
 * USB bus de-activation macro, required by the USB driver.
 */
#define usb_lld_disconnect_bus(usbp) palSetPad(GPIOC, GPIOC_USB_DISC)

/*
 * LED on/off control
 */

#define LEDS_PORT GPIOB
#define DATA_RX GPIOB_LED_DATA_RX
#define DATA_TX GPIOB_LED_DATA_TX
#define STATUS GPIOB_LED_STATUS

#define boardLedOn(led) palClearPad(GPIOB, led)
#define boardLedOff(led) palSetPad(GPIOB, led)

#define RXLED_PORT GPIOB
#define RXLED_PAD GPIOB_LED_DATA_TX
#define TXLED_PORT GPIOB
#define TXLED_PAD GPIOB_LED_DATA_RX

/*
 * Radio functions
 */

#define RF22_SPI SPID1
#define RF22_SPI_CFG SPI_CR1_BR_0
#define RF22_NSS_PORT GPIOA
#define RF22_NSS_PAD GPIOA_SPI1NSS
#define RF22_SDN_PORT GPIOA
#define RF22_SDN_PAD 2
#define RF22_IRQ_PORT GPIOA
#define RF22_IRQ_PAD 3
#define RF22_IRQ_EXT EXT_MODE_GPIOA

#define RF24_SPI SPID2
#define RF24_SPI_CFG SPI_CR1_BR_0
#define RF24_NSS_PORT GPIOB
#define RF24_NSS_PAD GPIOB_SPI2NSS
#define RF24_CE_PORT GPIOC
#define RF24_CE_PAD 10
#define RF24_IRQ_PORT GPIOC
#define RF24_IRQ_PAD 11
#define RF24_IRQ_EXT EXT_MODE_GPIOC

#if !defined(_FROM_ASM_)
#ifdef __cplusplus
extern "C" {
#endif
  void boardInit(void);
#ifdef __cplusplus
}
#endif
#endif /* _FROM_ASM_ */

#endif /* _BOARD_H_ */
