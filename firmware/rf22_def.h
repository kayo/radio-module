#define RF22_SPI_WRITE_MASK 0x80
#define RF22_TX_FIFO_LENGTH 0x40
#define RF22_RX_FIFO_LENGTH 0x40

/* Registers */
#define RF22_REG_DEVICE_TYPE                         0x00
#define RF22_REG_VERSION_CODE                        0x01
#define RF22_REG_DEVICE_STATUS                       0x02
#define RF22_REG_INTERRUPT_STATUS1                   0x03
#define RF22_REG_INTERRUPT_STATUS2                   0x04
#define RF22_REG_INTERRUPT_ENABLE1                   0x05
#define RF22_REG_INTERRUPT_ENABLE2                   0x06
#define RF22_REG_OPERATING_MODE1                     0x07
#define RF22_REG_OPERATING_MODE2                     0x08
#define RF22_REG_OSCILLATOR_LOAD_CAPACITANCE         0x09
#define RF22_REG_UC_OUTPUT_CLOCK                     0x0a
#define RF22_REG_GPIO_CONFIGURATION0                 0x0b
#define RF22_REG_GPIO_CONFIGURATION1                 0x0c
#define RF22_REG_GPIO_CONFIGURATION2                 0x0d
#define RF22_REG_IO_PORT_CONFIGURATION               0x0e
#define RF22_REG_ADC_CONFIGURATION                   0x0f
#define RF22_REG_ADC_SENSOR_AMP_OFFSET               0x10
#define RF22_REG_ADC_VALUE                           0x11
#define RF22_REG_TEMPERATURE_SENSOR_CALIBRATION      0x12
#define RF22_REG_TEMPERATURE_VALUE_OFFSET            0x13
#define RF22_REG_WAKEUP_TIMER_PERIOD1                0x14
#define RF22_REG_WAKEUP_TIMER_PERIOD2                0x15
#define RF22_REG_WAKEUP_TIMER_PERIOD3                0x16
#define RF22_REG_WAKEUP_TIMER_VALUE1                 0x17
#define RF22_REG_WAKEUP_TIMER_VALUE2                 0x18
#define RF22_REG_LDC_MODE_DURATION                   0x19
#define RF22_REG_LOW_BATTERY_DETECTOR_THRESHOLD      0x1a
#define RF22_REG_BATTERY_VOLTAGE_LEVEL               0x1b
#define RF22_REG_IF_FILTER_BANDWIDTH                 0x1c
#define RF22_REG_AFC_LOOP_GEARSHIFT_OVERRIDE         0x1d
#define RF22_REG_AFC_TIMING_CONTROL                  0x1e
#define RF22_REG_CLOCK_RECOVERY_GEARSHIFT_OVERRIDE   0x1f
#define RF22_REG_CLOCK_RECOVERY_OVERSAMPLING_RATE    0x20
#define RF22_REG_CLOCK_RECOVERY_OFFSET2              0x21
#define RF22_REG_CLOCK_RECOVERY_OFFSET1              0x22
#define RF22_REG_CLOCK_RECOVERY_OFFSET0              0x23
#define RF22_REG_CLOCK_RECOVERY_TIMING_LOOP_GAIN1    0x24
#define RF22_REG_CLOCK_RECOVERY_TIMING_LOOP_GAIN0    0x25
#define RF22_REG_RSSI                                0x26
#define RF22_REG_RSSI_THRESHOLD                      0x27
#define RF22_REG_ANTENNA_DIVERSITY1                  0x28
#define RF22_REG_ANTENNA_DIVERSITY2                  0x29
#define RF22_REG_AFC_LIMITER                         0x2a
#define RF22_REG_AFC_CORRECTION_READ                 0x2b
#define RF22_REG_OOK_COUNTER_VALUE_1                 0x2c
#define RF22_REG_OOK_COUNTER_VALUE_2                 0x2d
#define RF22_REG_SLICER_PEAK_HOLD                    0x2e
#define RF22_REG_DATA_ACCESS_CONTROL                 0x30
#define RF22_REG_EZMAC_STATUS                        0x31
#define RF22_REG_HEADER_CONTROL1                     0x32
#define RF22_REG_HEADER_CONTROL2                     0x33
#define RF22_REG_PREAMBLE_LENGTH                     0x34
#define RF22_REG_PREAMBLE_DETECTION_CONTROL1         0x35
#define RF22_REG_SYNC_WORD3                          0x36
#define RF22_REG_SYNC_WORD2                          0x37
#define RF22_REG_SYNC_WORD1                          0x38
#define RF22_REG_SYNC_WORD0                          0x39
#define RF22_REG_TRANSMIT_HEADER3                    0x3a
#define RF22_REG_TRANSMIT_HEADER2                    0x3b
#define RF22_REG_TRANSMIT_HEADER1                    0x3c
#define RF22_REG_TRANSMIT_HEADER0                    0x3d
#define RF22_REG_TRANSMIT_PACKET_LENGTH              0x3e
#define RF22_REG_CHECK_HEADER3                       0x3f
#define RF22_REG_CHECK_HEADER2                       0x40
#define RF22_REG_CHECK_HEADER1                       0x41
#define RF22_REG_CHECK_HEADER0                       0x42
#define RF22_REG_HEADER_ENABLE3                      0x43
#define RF22_REG_HEADER_ENABLE2                      0x44
#define RF22_REG_HEADER_ENABLE1                      0x45
#define RF22_REG_HEADER_ENABLE0                      0x46
#define RF22_REG_RECEIVED_HEADER3                    0x47
#define RF22_REG_RECEIVED_HEADER2                    0x48
#define RF22_REG_RECEIVED_HEADER1                    0x49
#define RF22_REG_RECEIVED_HEADER0                    0x4a
#define RF22_REG_RECEIVED_PACKET_LENGTH              0x4b
#define RF22_REG_ANALOG_TEST_BUS_SELECT              0x50
#define RF22_REG_DIGITAL_TEST_BUS_SELECT             0x51
#define RF22_REG_TX_RAMP_CONTROL                     0x52
#define RF22_REG_PLL_TUNE_TIME                       0x53
#define RF22_REG_CALIBRATION_CONTROL                 0x55
#define RF22_REG_MODEM_TEST                          0x56
#define RF22_REG_CHARGE_PUMP_TEST                    0x57
#define RF22_REG_CHARGE_PUMP_CURRENT_TRIMMING        0x58
#define RF22_REG_DIVIDER_CURRENT_TRIMMING            0x59
#define RF22_REG_VCO_CURRENT_TRIMMING                0x5a
#define RF22_REG_VCO_CALIBRATION                     0x5b
#define RF22_REG_SYNTHESIZER_TEST                    0x5c
#define RF22_REG_BLOCK_ENABLE_OVERRIDE1              0x5d
#define RF22_REG_BLOCK_ENABLE_OVERRIDE2              0x5e
#define RF22_REG_BLOCK_ENABLE_OVERRIDE3              0x5f
#define RF22_REG_CHANNEL_FILTER_COEFFICIENT_ADDRESS  0x60
#define RF22_REG_CHANNEL_FILTER_COEFFICIENT_VALUE    0x61
#define RF22_REG_CRYSTAL_OSCILLATOR_POR_CONTROL      0x62
#define RF22_REG_RC_OSCILLATOR_COARSE_CALIBRATION    0x63
#define RF22_REG_RC_OSCILLATOR_FINE_CALIBRATION      0x64
#define RF22_REG_LDO_CONTROL_OVERRIDE                0x65
#define RF22_REG_LDO_LEVEL_SETTINGS                  0x66
#define RF22_REG_DELTA_SIGMA_ADC_TUNING1             0x67
#define RF22_REG_DELTA_SIGMA_ADC_TUNING2             0x68
#define RF22_REG_AGC_OVERRIDE1                       0x69
#define RF22_REG_AGC_OVERRIDE2                       0x6a
#define RF22_REG_GFSK_FIR_FILTER_COEFFICIENT_ADDRESS 0x6b
#define RF22_REG_GFSK_FIR_FILTER_COEFFICIENT_VALUE   0x6c
#define RF22_REG_TX_POWER                            0x6d
#define RF22_REG_TX_DATA_RATE1                       0x6e
#define RF22_REG_TX_DATA_RATE0                       0x6f
#define RF22_REG_MODULATION_CONTROL1                 0x70
#define RF22_REG_MODULATION_CONTROL2                 0x71
#define RF22_REG_FREQUENCY_DEVIATION                 0x72
#define RF22_REG_FREQUENCY_OFFSET1                   0x73
#define RF22_REG_FREQUENCY_OFFSET2                   0x74
#define RF22_REG_FREQUENCY_BAND_SELECT               0x75
#define RF22_REG_NOMINAL_CARRIER_FREQUENCY1          0x76
#define RF22_REG_NOMINAL_CARRIER_FREQUENCY0          0x77
#define RF22_REG_FREQUENCY_HOPPING_CHANNEL_SELECT    0x79
#define RF22_REG_FREQUENCY_HOPPING_STEP_SIZE         0x7a
#define RF22_REG_TX_FIFO_CONTROL1                    0x7c
#define RF22_REG_TX_FIFO_CONTROL2                    0x7d
#define RF22_REG_RX_FIFO_CONTROL                     0x7e
#define RF22_REG_FIFO_ACCESS_DATA                    0x7f

/* RF22_REG_DEVICE_TYPE                      0x00 */
#define RF22_DEVICE_TYPE_RX_TRX                 0x08
#define RF22_DEVICE_TYPE_TX                     0x07

/* RF22_REG_VERSION_CODE                     0x01 */
/* Si4430/31/32 Rev B1: 00110 */
/* Si100x Rev C, Si101x Rev A, Si102x/3x Rev A: 00110 */
#define RF22_VERSION_CODE_6                     0x06
/* Si100x Rev E, Si101x Rev B: Si102x/3x Rev B: 00111 */
#define RF22_VERSION_CODE_7                     0x07

/* RF22_REG_DEVICE_STATUS                    0x02 */
#define RF22_FIFO_OVERFLOW                      0x80
#define RF22_FIFO_UNDERFLOW                     0x40
#define RF22_RX_FIFO_EMPTY                      0x20
#define RF22_HEADER_ERROR                       0x10
#define RF22_FREQUENCY_ERROR                    0x08
#define RF22_LOCK_DETECT                        0x04
#define RF22_CHIP_POWER_STATE_MASK              0x03
#define RF22_CHIP_POWER_STATE_SHIFT             0x00
#define RF22_CHIP_IDLE                          0x00
#define RF22_CHIP_RX                            0x01
#define RF22_CHIP_TX                            0x10

/* RF22_REG_INTERRUPT_STATUS1                0x03 */
#define RF22_INT1_FIFO_UNDERFLOW_OVERFLOW       0x80
#define RF22_INT1_TX_FIFO_ALMOST_FULL           0x40
#define RF22_INT1_TX_FIFO_ALMOST_EMPTY          0x20
#define RF22_INT1_RX_FIFO_ALMOST_FULL           0x10
#define RF22_INT1_EXTERNAL                      0x08
#define RF22_INT1_PACKET_SENT                   0x04
#define RF22_INT1_VALID_PACKET_RECEIVED         0x02
#define RF22_INT1_CRC_ERROR                     0x01

/* RF22_REG_INTERRUPT_STATUS2                0x04 */
#define RF22_INT2_SYNC_WORD_DETECTED            0x80
#define RF22_INT2_VALID_PREAMBLE_DETECTED       0x40
#define RF22_INT2_INVALID_PREAMBLE_DETECTED     0x20
#define RF22_INT2_RSSI                          0x10
#define RF22_INT2_WAKEUP_TIMER                  0x08
#define RF22_INT2_LOW_BATTERY_DETECTED          0x04
#define RF22_INT2_CHIP_READY                    0x02
#define RF22_INT2_POWER_ON_RESET                0x01

/* RF22_REG_INTERRUPT_ENABLE1                0x05 */
#define RF22_EN1_NONE                           0x00
#define RF22_EN1_FIFO_UNDERFLOW_OVERFLOW        0x80
#define RF22_EN1_TX_FIFO_ALMOST_FULL            0x40
#define RF22_EN1_TX_FIFO_ALMOST_EMPTY           0x20
#define RF22_EN1_RX_FIFO_ALMOST_FULL            0x10
#define RF22_EN1_EXTERNAL                       0x08
#define RF22_EN1_PACKET_SENT                    0x04
#define RF22_EN1_VALID_PACKET_RECEIVED          0x02
#define RF22_EN1_CRC_ERROR                      0x01

/* RF22_REG_INTERRUPT_ENABLE2                0x06 */
#define RF22_EN2_NONE                           0x00
#define RF22_EN2_SYNC_WORD_DETECTED             0x80
#define RF22_EN2_VALID_PREAMBLE_DETECTED        0x40
#define RF22_EN2_INVALID_PREAMBLE_DETECTED      0x20
#define RF22_EN2_RSSI                           0x10
#define RF22_EN2_WAKEUP_TIMER                   0x08
#define RF22_EN2_LOW_BATTERY_DETECTED           0x04
#define RF22_EN2_CHIP_READY                     0x02
#define RF22_EN2_POWER_ON_RESET                 0x01

/* RF22_REG_OPERATING_MODE1                  0x07 */
#define RF22_OP1_SOFTWARE_REGISTER_RESET        0x80
#define RF22_OP1_ENABLE_LOW_BATTERY_DETECT      0x40
#define RF22_OP1_ENABLE_WAKEUP_TIMER            0x20
#define RF22_OP1_32_768KHZ_CRYSTAL_SELECT       0x10
#define RF22_OP1_MANUAL_TRANSMIT_MODE_TX_ON     0x08
#define RF22_OP1_MANUAL_RECEIVER_MODE_RX_ON     0x04
#define RF22_OP1_TUNE_MODE_PLL_ON               0x02
#define RF22_OP1_READY_MODE_CRYSTAL_ON          0x01

/* RF22_REG_OPERATING_MODE2                  0x08 */
#define RF22_OP2_ANTENNA_DIVERSITY_MASK         0xc0
#define RF22_OP2_ANTENNA_DIVERSITY_OFFSET       0x05
#define RF22_OP2_RX_MULTI_PACKET                0x10
#define RF22_OP2_AUTOMATIC_TRANSMISSION         0x08
#define RF22_OP2_ENABLE_LOW_DUTY_CYCLE_MODE     0x04
#define RF22_OP2_RX_FIFO_RESET_CLEAR            0x02
#define RF22_OP2_TX_FIFO_RESET_CLEAR            0x01

/* RF22_REG_OSCILLATOR_LOAD_CAPACITANCE */
#define RF22_CRYSTAL_SHIFT                      0x80
#define RF22_CRYSTAL_LOAD_CAPACITANCE_MASK      0x7f

/* RF22_REG_UC_OUTPUT_CLOCK */

/* RF22_REG_GPIO_CONFIGURATION0/1/2 */
#define RF22_GPIO_DRIVING_CAPABILITY_MASK       0xc0
#define RF22_GPIO_DRIVING_CAPABILITY_SHIFT      0x06
#define RF22_GPIO_ENABLE_200K_PULLUP_RESISTOR   0x20
#define RF22_GPIO_FUNCTION_SELECT_MASK          0x1f
#define RF22_GPIO_FUNCTION_SELECT_SHIFT         0x00

#define RF22_GPIO_WAKEUP_TIMER                  0x01
#define RF22_GPIO_LOW_BATTERY_DETECT            0x02
#define RF22_GPIO_DIRECT_DIGITAL_INPUT          0x03
#define RF22_GPIO_EXT_INTERRUPT_FALLING_EDGE    0x04
#define RF22_GPIO_EXT_INTERRUPT_RISING_EDGE     0x05
#define RF22_GPIO_EXT_INTERRUPT_STATE_CHANGE    0x06
#define RF22_GPIO_ADC_ANALOG_INPUT              0x07
#define RF22_GPIO_ANALOG_TEST_N_INPUT           0x08
#define RF22_GPIO_ANALOG_TEST_P_INPUT           0x09
#define RF22_GPIO_DIRECT_DIGITAL_OUTPUT         0x0a
#define RF22_GPIO_DIGITAL_TEST_OUTPUT           0x0b
#define RF22_GPIO_ANALOG_TEST_N_OUTPUT          0x0c
#define RF22_GPIO_ANALOG_TEST_P_OUTPUT          0x0d
#define RF22_GPIO_REFERENCE_VOLTAGE_OUTPUT      0x0e
#define RF22_GPIO_TX_RX_DATA_CLOCK_OUTPUT       0x0f
#define RF22_GPIO_DIRECT_MODE_TX_DATA_INPUT     0x10
#define RF22_GPIO_EXT_RETRANSMISSION_REQUEST    0x11
#define RF22_GPIO_TX_STATE_OUTPUT               0x12
#define RF22_GPIO_TX_FIFO_ALMOST_FULL_OUTPUT    0x13
#define RF22_GPIO_RX_DATA_OUTPUT                0x14
#define RF22_GPIO_RX_STATE_OUTPUT               0x15
#define RF22_GPIO_RX_FIFO_ALMOST_FULL           0x16
#define RF22_GPIO_ANTENNA1_SWITCH_OUTPUT        0x17
#define RF22_GPIO_ANTENNA2_SWITCH_OUTPUT        0x18
#define RF22_GPIO_VALID_PREAMBLE_DETECTED       0x19
#define RF22_GPIO_INVALID_PREAMBLE_DETECTED     0x1a
#define RF22_GPIO_SYNC_WORD_DETECTED            0x1b
#define RF22_GPIO_CLEAR_CHANNEL_ASSESSMENT      0x1c
#define RF22_GPIO_VDD                           0x1d
#define RF22_GPIO_GND                           0x1e

/* RF22_REG_GPIO_CONFIGURATION0 */
#define RF22_GPIO0_POWER_ON_RESET               0x00

/* RF22_REG_GPIO_CONFIGURATION1 */
#define RF22_GPIO1_INVERTED_POWER_ON_RESET      0x00

/* RF22_REG_GPIO_CONFIGURATION2 */
#define RF22_GPIO2_MICROCONTROLLER_CLOCK        0x00

/* RF22_REG_IO_PORT_CONFIGURATION */

/* RF22_REG_ADC_CONFIGURATION                0x0f */
#define RF22_ADC_MEASUREMENT_START              0x80
#define RF22_ADC_MEASUREMENT_DONE               0x80
#define RF22_ADC_SOURCE_SELECTION_MASK          0x70
#define RF22_ADC_INTERNAL_TEMPERATURE_SENSOR    0x00
#define RF22_ADC_GPIO0_SINGLE_ENDED             0x10
#define RF22_ADC_GPIO1_SINGLE_ENDED             0x20
#define RF22_ADC_GPIO2_SINGLE_ENDED             0x30
#define RF22_ADC_GPIO0_GPIO1_DIFFERENTIAL       0x40
#define RF22_ADC_GPIO1_GPIO2_DIFFERENTIAL       0x50
#define RF22_ADC_GPIO0_GPIO2_DIFFERENTIAL       0x60
#define RF22_ADC_GND                            0x70
#define RF22_ADC_REFERENCE_SELECTION_MASK       0x0c
#define RF22_ADC_BANDGAP_VOLTAGE_1_2V           0x00
#define RF22_ADC_VDD_DIV_3                      0x08
#define RF22_ADC_VDD_DIV_2                      0x0c
#define RF22_ADC_SOURCE_AMPLIFIER_GAIN_MASK     0x03

/* RF22_REG_ADC_SENSOR_AMP_OFFSET            0x10 */
#define RF22_ADC_SENSOR_AMPLIFIER_OFFSET_MASK   0x0f

/* RF22_REG_TEMPERATURE_SENSOR_CALIBRATION   0x12 */
#define RF22_TEMP_SENSOR_RANGE_MASK             0xc0
#define RF22_TEMP_SENSOR_RANGE_M64_64C          0x00
#define RF22_TEMP_SENSOR_RANGE_M64_192C         0x40
#define RF22_TEMP_SENSOR_RANGE_0_128C           0x80
#define RF22_TEMP_SENSOR_RANGE_M40_216F         0xc0
#define RF22_TEMP_SENSOR_KELVIN_TO_CELSIUS      0x20
#define RF22_TEMP_SENSOR_TRIM_ENABLE            0x10
#define RF22_TEMP_SENSOR_TRIM_VALUE             0x0f

/* RF22_REG_WAKEUP_TIMER_PERIOD1             0x14 */
#define RF22_WTR                                0x3c
#define RF22_WTD                                0x03

/* RF22_REG_IF_FILTER_BANDWIDTH              0x1c */
#define RF22_BYPASS_DECIMATE_BY_3_STAGE         0x80
#define RF22_IF_FILTER_DECIMATION_RATES_MASK    0x70
#define RF22_IF_FILTER_DECIMATION_RATES_SHIFT   0x04
#define RF22_IF_FILTER_COEFFICIENT_SETS_MASK    0x0f
#define RF22_IF_FILTER_COEFFICIENT_SETS_SHIFT   0x00

/* RF22_REG_AFC_LOOP_GEARSHIFT_OVERRIDE      0x1d */
#define RF22_AFC_WIDEBAND_ENABLE                0x80
#define RF22_AFC_ENABLE                         0x40
#define RF22_AFC_GEAR_HIGH_MASK                 0x38
#define RF22_AFC_GEAR_LOW_MASK                  0x07
#define RF22_AFC_HIGH_GEAR_SETTING_MASK         0x38
#define RF22_AFC_HIGH_GEAR_SETTING_SHIFT        0x03
#define RF22_AFC_1P5DB_OR_0DB                   0x04
#define RF22_AFC_MATAP                          0x02
#define RF22_AFC_PH0SIZE                        0x02

/* RF22_REG_AFC_TIMING_CONTROL               0x1e */
#define RF22_SWAIT_TIMER_MASK                   0xc0
#define RF22_SWAIT_TIMER_OFFSET                 0x06
#define RF22_SHWAIT_MASK                        0x38
#define RF22_SHWAIT_OFFSET                      0x03
#define RF22_ANWAIT_MASK                        0x07
#define RF22_ANWAIT_OFFSET                      0x00

/* RF22_REG_CLOCK_RECOVERY_GEARSHIFT_OVERRIDE 0x1f */
#define RF22_CLOCK_RECOVERY_FAST_GEARSHIFT_MASK  0x38
#define RF22_CLOCK_RECOVERY_FAST_GEARSHIFT_SHIFT 0x03
#define RF22_CLOCK_RECOVERY_SLOW_GEARSHIFT_MASK  0x07
#define RF22_CLOCK_RECOVERY_SLOW_GEARSHIFT_SHIFT 0x00

/* RF22_REG_CLOCK_RECOVERY_OFFSET2              0x21 */
#define RF22_OVERSAMPLING_RATE_MSB_MASK         0xe0
#define RF22_OVERSAMPLING_RATE_MSB_SHIFT        0x05
#define RF22_SKIP_2ND_PHASE_ANT_DIV_THRESHOLD   0x10
#define RF22_NCO_OFFSET_MSB_MASK                0x0f
#define RF22_NCO_OFFSET_MSB_SHIFT               0x00

/* RF22_REG_CLOCK_RECOVERY_TIMING_LOOP_GAIN1    0x24 */
#define RF22_HIGH_DATA_RATE_RX_COMPENSATION     0x10
#define RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_2X 0x08
#define RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_MSB_MASK 0x07
#define RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_MSB_SHIFT 0x00

/* RF22_REG_DATA_ACCESS_CONTROL              0x30 */
#define RF22_ENABLE_PACKET_RX_HANDLING          0x80
#define RF22_LSB_FIRST_ENBALE                   0x40
#define RF22_CRC_DATA_ONLY_ENABLE               0x20
#define RF22_SKIP_2ND_PHASE_PREAMBLE_DETECTION  0x10
#define RF22_ENABLE_PACKET_TX_HANDLING          0x08
#define RF22_ENABLE_CRC                         0x04
#define RF22_CRC_POLYNOMIAL_SELECTION_MASK      0x03
#define RF22_CRC_POLYNOMIAL_SELECTION_SHIFT     0x00
#define RF22_CRC_CCITT                          0x00
#define RF22_CRC_16IBM                          0x01
#define RF22_CRC_16IEC                          0x02
#define RF22_CRC_BIACHEVA                       0x03

/* RF22_REG_HEADER_CONTROL1                  0x32 */
#define RF22_BROADCAST_ADDR_CHECK_MASK          0xf0
#define RF22_BROADCAST_ADDR_CHECK_SHIFT         0x04
#define RF22_BROADCAST_ADDR_CHECK_NONE          0x00
#define RF22_BROADCAST_ADDR_CHECK_HEADER0       0x10
#define RF22_BROADCAST_ADDR_CHECK_HEADER1       0x20
#define RF22_BROADCAST_ADDR_CHECK_HEADER2       0x40
#define RF22_BROADCAST_ADDR_CHECK_HEADER3       0x80
#define RF22_RECEIVED_BYTES_TO_CHECK_MASK       0x0f
#define RF22_RECEIVED_BYTES_TO_CHECK_SHIFT      0x00
#define RF22_RECEIVED_BYTES_TO_CHECK_NONE       0x00
#define RF22_RECEIVED_BYTES_TO_CHECK_HEADER0    0x01
#define RF22_RECEIVED_BYTES_TO_CHECK_HEADER1    0x02
#define RF22_RECEIVED_BYTES_TO_CHECK_HEADER2    0x04
#define RF22_RECEIVED_BYTES_TO_CHECK_HEADER3    0x08

/* RF22_REG_HEADER_CONTROL2                  0x33 */
#define RF22_SKIP_SYNC_WORD_SEARCH_TIMEOUT      0x80
#define RF22_HEADER_LENGTH_MASK                 0x70
#define RF22_HEADER_LENGTH_SHIFT                0x04
#define RF22_FIX_PACKET_LENGTH                  0x08
#define RF22_SYNC_WORD_LENGTH_MASK              0x06
#define RF22_SYNC_WORD_LENGTH_SHIFT             0x01
#define RF22_MSB_OF_PREAMBLE_LENGTH             0x01

/* RF22_REG_PREAMBLE_DETECTION_CONTROL1      0x35 */
#define RF22_PREAMBLE_DETECTION_THRESHOLD_MASK  0xf8
#define RF22_PREAMBLE_DETECTION_THRESHOLD_SHIFT 0x03
#define RF22_RSSI_OFFSET_MASK                   0x03
#define RF22_RSSI_OFFSET_SHIFT                  0x00

/* RF22_REG_AGC_OVERRIDE1                    0x69 */
#define RF22_AGC_GAIN_INC_DURING_SIGNAL_REDUCTS 0x40
#define RF22_AGC_ENABLE                         0x20
#define RF22_AGC_LNA_GAIN_MIN_5DB               0x00
#define RF22_AGC_LNA_GAIN_MAX_25DB              0x10
#define RF22_PGA_GAIN_VALUE_MASK                0x0f
#define RF22_PGA_GAIN_VALUE_SHIFT               0x00

/* RF22_REG_TX_POWER                         0x6d */
/* https://www.sparkfun.com/datasheets/Wireless/General/RFM22B.pdf */
#define RF22_PAPEAKVAL                          0x80
#define RF22_PAPEAKEN                           0x40
#define RF22_PAPEAKLVL                          0x30
#define RF22_PAPEAKLVL6_5                       0x00
#define RF22_PAPEAKLVL7                         0x10
#define RF22_PAPEAKLVL7_5                       0x20
#define RF22_PAPEAKLVL8                         0x30
#define RF22_LNA_SW                             0x08
#define RF22_TX_POWER                           0x07
#define RF22_TX_POWER_4X31                      0x08 /* Not used in RFM22B */
/* For RFM22B: */
#define RF22_TX_POWER_1DBM                      0x00
#define RF22_TX_POWER_2DBM                      0x01
#define RF22_TX_POWER_5DBM                      0x02
#define RF22_TX_POWER_8DBM                      0x03
#define RF22_TX_POWER_11DBM                     0x04
#define RF22_TX_POWER_14DBM                     0x05 
#define RF22_TX_POWER_17DBM                     0x06 
#define RF22_TX_POWER_20DBM                     0x07 
/* RFM23B only: */
#define RF22_RF23B_TX_POWER_M8DBM               0x00 /* -8dBm */
#define RF22_RF23B_TX_POWER_M5DBM               0x01 /* -5dBm */
#define RF22_RF23B_TX_POWER_M2DBM               0x02 /* -2dBm */
#define RF22_RF23B_TX_POWER_1DBM                0x03 /* 1dBm */
#define RF22_RF23B_TX_POWER_4DBM                0x04 /* 4dBm */
#define RF22_RF23B_TX_POWER_7DBM                0x05 /* 7dBm */
#define RF22_RF23B_TX_POWER_10DBM               0x06 /* 10dBm */
#define RF22_RF23B_TX_POWER_13DBM               0x07 /* 13dBm */
/* RFM23BP only: */
#define RF22_RF23BP_TX_POWER_28DBM              0x05 /* 28dBm */
#define RF22_RF23BP_TX_POWER_29DBM              0x06 /* 29dBm */
#define RF22_RF23BP_TX_POWER_30DBM              0x07 /* 30dBm */

/* RF22_REG_MODULATION_CONTROL1              0x70 */
#define RF22_ENABLE_DATA_WHITENING              0x01
#define RF22_ENABLE_MANCHESTER_ENCODING         0x02
#define RF22_ENABLE_MANCHESTER_DATA_INVERSION   0x04
#define RF22_MANCHESTER_PREAMBLE_POLARITY       0x08
#define RF22_ENABLE_PACKET_HANDLER_POWER_DOWN   0x10
#define RF22_TX_DATA_RATE_BELOW_30KBPS          0x20

/* RF22_REG_MODULATION_CONTROL2              0x71 */
#define RF22_TX_DATA_CLOCK_MASK                 0xc0
#define RF22_TX_DATA_CLOCK_SHIFT                0x06
#define RF22_TX_DATA_CLOCK_UNCLOCKED            0x00 /* for FSK and OOK only! */
#define RF22_TX_DATA_CLOCK_VIA_GPIO             0x40 /* one of the GPIO’s should be programmed as well! */
#define RF22_TX_DATA_CLOCK_VIA_SDO              0x80
#define RF22_TX_DATA_CLOCK_VIA_NIRQ             0xc0
#define RF22_DATA_SOURCE_MASK                   0x30
#define RF22_DATA_SOURCE_SHIFT                  0x04
#define RF22_DATA_SOURCE_DIRECT_GPIO            0x00
#define RF22_DATA_SOURCE_DIRECT_SDI             0x10
#define RF22_DATA_SOURCE_FIFO                   0x20
#define RF22_DATA_SOURCE_PN9                    0x30
#define RF22_ENABLE_DATA_INVERSION              0x08
#define RF22_FREQUENCY_DEVIATION_MSB            0x04
#define RF22_MODULATION_TYPE_MASK               0x03
#define RF22_MODULATION_TYPE_SHIFT              0x00
#define RF22_MODULATION_UNMODULATED             0x00
#define RF22_MODULATION_OOK                     0x01
#define RF22_MODULATION_FSK                     0x02
#define RF22_MODULATION_GFSK                    0x03 /* enable TX DATA CLOCK when direct mode is used */

/* RF22_REG_FREQUENCY_BAND_SELECT            0x75 */
#define RF22_SIDE_BAND_SELECT                   0x40
#define RF22_HIGH_BAND_SELECT                   0x20
#define RF22_FREQUENCY_BAND_SELECT_MASK         0x1f
