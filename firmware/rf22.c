#include "ch.h"
#include "hal.h"

#include "rf22_def.h"
#include "rf22.h"

#ifdef RF22_DEBUG
# include "chprintf.h"
extern SerialUSBDriver SDU1;
# define dbgPrint(fmt, args...) chprintf((BaseSequentialStream*)&SDU1, fmt "\r\n", ##args);

# ifdef RF22_DEBUG_REG
#  define dbgPrintReg(dir, reg, len, buf) {         \
    uint8_t i;                                      \
    for(i = 0; i < len; i++){                       \
      dbgPrint(#dir " %02x %02x", reg + i, buf[i]); \
    }                                               \
  }
# endif
#endif

#ifndef dbgPrint
# define dbgPrint(args...)
#endif

#ifndef dbgPrintReg
# define dbgPrintReg(args...)
#endif

static void _rf22AcquireSpi(RF22Driver *radio){
  const SPIConfig *spicfg = radio->config->spicfg;
  SPIDriver *spip = radio->config->spip;
  
  /* acquire ownership of the bus */
  spiAcquireBus(spip);
  /* setup transfer parameters */
  spiStart(spip, spicfg);
}

static void _rf22ReleaseSpi(RF22Driver *radio){
  SPIDriver *spip = radio->config->spip;
  
  /* shutdown spi */
  spiStop(spip);
  /* release ownership of the bus */
  spiReleaseBus(spip);
}

#if RF22_USE_MUTUAL_EXCLUSION || defined(__DOXYGEN__)
static void _rf22AcquireIface(RF22Driver *radio){
# if CH_USE_MUTEXES
  chMtxLock(&radio->mutex);
# elif CH_USE_SEMAPHORES
  chSemWait(&radio->semaphore);
# endif

  radio->thread = chThdSelf();
  
  _rf22AcquireSpi(radio);
}

static void _rf22ReleaseIface(RF22Driver *radio){
  _rf22ReleaseSpi(radio);
  
  radio->thread = NULL;
  
# if CH_USE_MUTEXES
  chMtxUnlock();
# elif CH_USE_SEMAPHORES
  chSemSignal(&radio->semaphore);
# endif
}
#endif

static void _rf22RegPut(RF22Driver *radio, uint8_t reg, RF22Length len, const RF22Buffer *buf){
  SPIDriver *spip = radio->config->spip;
  
  dbgPrintReg(W, reg, len, buf);
  
  reg |= RF22_SPI_WRITE_MASK;
  
  spiSelect(spip); /* Slave Select assertion. */
  spiSend(spip, 1, &reg); /* Send register address */
  spiSend(spip, len, buf); /* Send register value or buffer contents. */
  spiUnselect(spip); /* Slave Select de-assertion. */
}

static void _rf22RegPut1(RF22Driver *radio, uint8_t reg, uint8_t val){
  _rf22RegPut(radio, reg, 1, &val);
}

static void _rf22RegPutL(RF22Driver *radio, uint8_t reg, RF22Length len, RF22Header val){
  const uint8_t data[] = {
    val,
    val >> 8,
    val >> 16,
    val >> 24
  };
  
  _rf22RegPut(radio, reg, len, data);
}

static void _rf22RegGet(RF22Driver *radio, uint8_t reg, RF22Length len, RF22Buffer *buf){
  SPIDriver *spip = radio->config->spip;
  
  spiSelect(spip); /* Slave Select assertion. */
  spiSend(spip, 1, &reg); /* Send register address */
  spiReceive(spip, len, buf); /* Receive register value or buffer contents. */
  spiUnselect(spip); /* Slave Select de-assertion. */
  
  dbgPrintReg(R, reg, len, buf);
}

void rf22RegPut(RF22Driver *radio, uint8_t reg, RF22Length len, const RF22Buffer *buf){
  chDbgCheck(radio != NULL && radio->config != NULL &&
             radio->config->spip != NULL &&
             reg <= 0x7f && len <= 0x7f - reg && buf != NULL,
             "rf22RegPut");
  
  _rf22AcquireSpi(radio);
  
  _rf22RegPut(radio, reg, len, buf);

  _rf22ReleaseSpi(radio);
}

void rf22RegGet(RF22Driver *radio, uint8_t reg, RF22Length len, RF22Buffer *buf){
  chDbgCheck(radio != NULL && radio->config != NULL &&
             radio->config->spip != NULL &&
             reg <= 0x7f && len <= 0x7f - reg && buf != NULL,
             "rf22RegGet");

  _rf22AcquireSpi(radio);
  
  _rf22RegGet(radio, reg, len, buf);

  _rf22ReleaseSpi(radio);
}

/*
 * Device events handling
 */

void rf22SendEventFromIsr(RF22Driver *radio){
  chSysLockFromIsr();
  chSemSignalI(&radio->eventSemaphore);
  chSysUnlockFromIsr();
}

static void _rf22MaskEvents(RF22Driver *radio, const uint8_t poll[2]){
  _rf22RegPut(radio, RF22_REG_INTERRUPT_ENABLE1, 2, poll);
}

static const uint8_t _rf22NoEventsMask[] = {
  RF22_EN1_NONE,
  RF22_EN2_NONE,
};

static inline void _rf22UnmaskEvents(RF22Driver *radio){
  _rf22MaskEvents(radio, _rf22NoEventsMask);
}

static msg_t _rf22PollEventsTimeout(RF22Driver *radio, uint8_t polled[2], systime_t timeout){
  /* release spi bus until interrupt occurs */
  _rf22ReleaseSpi(radio);
  
  msg_t status = chSemWaitTimeout(&radio->eventSemaphore, timeout);
  
  /* acquire spi bus back for communicating through it */
  _rf22AcquireSpi(radio);
  
  if(status == RDY_OK){
    _rf22RegGet(radio, RF22_REG_INTERRUPT_STATUS1, 2, polled);
  }
  
  return status;
}

static inline msg_t _rf22PollEvents(RF22Driver *radio, uint8_t polled[2]){
  return _rf22PollEventsTimeout(radio, polled, TIME_INFINITE);
}

static inline void _rf22GetStatus(RF22Driver *radio, uint8_t *status){
  _rf22RegGet(radio, RF22_REG_DEVICE_STATUS, 1, status);
}

static inline void _rf22GetEZMacStatus(RF22Driver *radio, uint8_t *status){
  _rf22RegGet(radio, RF22_REG_EZMAC_STATUS, 1, status);
}

/*
 * Frequency/Channel setting
 */

static void _rf22PutFrequency(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  /* chDbgCheck(radio != NULL && config != NULL, "_rf22PutFrequency"); */
  
  chDbgAssert(config->carrierFrequency >= 240000000 &&
              config->carrierFrequency <= 960000000,
              "_rf22PutFrequency #value", "out of range");

  uint32_t frequency = config->carrierFrequency / 10 - 24000000;
  
  bool_t sideBandSelect = TRUE;
  bool_t highBandSelect = FALSE;
  
  if(frequency >= 24000000){
    highBandSelect = TRUE;
    frequency -= 24000000;
  }
  
  uint8_t frequencyBand = frequency / 1000000;
  uint16_t frequencyCarrier = (frequency - frequencyBand * 1000000) * 64 / 1000;
  
  uint8_t fr[] = {
    0,
    0,
    (sideBandSelect ? RF22_SIDE_BAND_SELECT : 0) |
    (highBandSelect ? RF22_HIGH_BAND_SELECT : 0) |
    (frequencyBand & RF22_FREQUENCY_BAND_SELECT_MASK),
    frequencyCarrier >> 8,
    frequencyCarrier & 0xff
  };
  
  _rf22RegPut(radio, RF22_REG_FREQUENCY_OFFSET1, sizeof(fr), fr);
  
  uint8_t status;
  _rf22GetStatus(radio, &status);
  
  chDbgAssert(!(status & RF22_FREQUENCY_ERROR), "_rf22PutFrequency", "cant set frequency");
}

static void _rf22PutChannelStep(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  _rf22RegPut(radio, RF22_REG_FREQUENCY_HOPPING_STEP_SIZE, 1, &config->hoppingStep);
}

static void _rf22PutChannel(RF22Driver *radio){
  _rf22RegPut(radio, RF22_REG_FREQUENCY_HOPPING_CHANNEL_SELECT, 1, &radio->hoppingChannel);
}

void rf22SetChannel(RF22Driver *radio, uint8_t channel){
  chDbgCheck(radio != NULL, "rf22SetChannel");

  chSysLock();
  
  chDbgAssert(radio->state != RF22_STOP, "rf22SetChannel()", "invalid state");
  
  radio->hoppingChannel = channel;
  
  chSysUnlock();
  
  if(radio->state != RF22_ERROR){
    _rf22AcquireSpi(radio);
    
    _rf22PutChannel(radio);

    _rf22ReleaseSpi(radio);
  }
}

/*
 * Modulation setting
 */
#if RF22_USE_PREDEF_MODEM_CONFIG

static void _rf22PutAFCPullInRange(RF22Driver *radio){
  const RF22Config *config = radio->config;

  if(config->afcPullInRange == RF22AfcPullInRangeDefault){
    return;
  }
  
  chDbgAssert((config->carrierFrequency >= 480000000 &&
               config->afcPullInRange <= 159375) ||
              (config->afcPullInRange <= 318750),
              "_rf22PutAFCPullInRange #value", "out of range");
  
  uint8_t afcLimiter = (config->afcPullInRange >> (config->carrierFrequency >= 480000000 ? 1 : 0)) / 625;
  
  _rf22RegPut(radio, RF22_REG_AFC_LIMITER, 1, &afcLimiter);
}

static void _rf22PutModulation(RF22Driver *radio){
  const RF22Config *config = radio->config;

  _rf22RegPut(radio, RF22_REG_IF_FILTER_BANDWIDTH, 1, &config->modem.if_filter_bandwidth);
  _rf22RegPut(radio, RF22_REG_CLOCK_RECOVERY_GEARSHIFT_OVERRIDE, sizeof(config->modem.clock_recovery), config->modem.clock_recovery);
  _rf22RegPut(radio, RF22_REG_OOK_COUNTER_VALUE_1, sizeof(config->modem.ook_counter_value), config->modem.ook_counter_value);
  _rf22RegPut(radio, RF22_REG_CHARGE_PUMP_CURRENT_TRIMMING, 1, &config->modem.change_pump_current_trimming);
  _rf22RegPut(radio, RF22_REG_AGC_OVERRIDE1, 1, &config->modem.agc_override1);
  _rf22RegPut(radio, RF22_REG_TX_DATA_RATE1, sizeof(config->modem.tx_data_rate), config->modem.tx_data_rate);
  _rf22RegPut(radio, RF22_REG_MODULATION_CONTROL1, sizeof(config->modem.modulation_control), config->modem.modulation_control);
  
  _rf22PutAFCPullInRange(radio);
}

#else/*RF22_USE_PREDEF_MODEM_CONFIG*/

typedef PACK_STRUCT_BEGIN struct { /* 32 bits */
  unsigned int band_width:16; /* ×100 [Hz] */
  unsigned int ndec_exp:3;
  unsigned int filset:4;
  unsigned int dwn3_bypass:1;
  unsigned int afc_limiter:8;
} PACK_STRUCT_STRUCT _RF22BWTableRow PACK_STRUCT_END;

static const _RF22BWTableRow _rf22BWTableH1[] = { /* Modulation Index H=1 */
#if RF22_USE_BW_TABLES
  {26, 5, 1, 0, 1},
  {28, 5, 2, 0, 1},
  {31, 5, 3, 0, 1},
  {32, 5, 4, 0, 1},
  {37, 5, 5, 0, 1},
  {49, 4, 1, 0, 2},
  {54, 4, 2, 0, 2},
  {59, 4, 3, 0, 2},
  {61, 4, 4, 0, 2},
  {72, 4, 5, 0, 3},
  {95, 3, 1, 0, 3},
  {106, 3, 2, 0, 4},
  {115, 3, 3, 0, 4},
  {121, 3, 4, 0, 4},
  {142, 3, 5, 0, 5},
  {162, 3, 6, 0, 6},
  {175, 3, 10, 0, 6},
  {194, 3, 11, 0, 7},
  {214, 3, 12, 0, 8},
  {239, 3, 13, 0, 8},
  {257, 3, 14, 0, 9},
  {282, 2, 5, 0, 10},
  {322, 2, 6, 0, 11},
  {347, 2, 7, 0, 12},
  {386, 2, 11, 0, 14},
  {427, 2, 12, 0, 15},
  {477, 2, 13, 0, 17},
  {512, 2, 14, 0, 18},
  {562, 1, 5, 0, 20},
  {641, 1, 6, 0, 23},
  {692, 1, 7, 0, 25},
  {752, 0, 1, 0, 27},
  {832, 0, 2, 0, 30},
  {900, 0, 3, 0, 32},
  {953, 0, 4, 0, 34},
  {1121, 0, 5, 0, 40},
  {1279, 0, 6, 0, 45},
  {1379, 0, 7, 0, 49},
  {1428, 1, 4, 1, 51},
  {1678, 1, 5, 1, 60},
  {1811, 1, 9, 1, 64},
  {1915, 1, 6, 1, 68},
  {2084, 1, 10, 1, 72},
  {2251, 0, 1, 1, 80},
  {2488, 0, 2, 1, 88},
  {2693, 0, 3, 1, 96},
  {2849, 0, 4, 1, 101},
  {3355, 0, 8, 1, 119},
  {3618, 0, 9, 1, 129},
  {4202, 0, 10, 1, 149},
  {4684, 0, 11, 1, 167},
  {5188, 0, 12, 1, 184},
  {5770, 0, 13, 1, 205},
  {6207, 0, 14, 1, 221},
#endif
};
static const _RF22BWTableRow _rf22BWTableH2[] = { /* Modulation Index 2<=H<10 */
#if RF22_USE_BW_TABLES
  {26, 5, 1, 0, 1},
  {28, 5, 2, 0, 1},
  {31, 5, 3, 0, 1},
  {32, 5, 4, 0, 1},
  {37, 5, 5, 0, 1},
  {49, 4, 1, 0, 2},
  {54, 4, 2, 0, 2},
  {59, 4, 3, 0, 2},
  {61, 4, 4, 0, 2},
  {72, 4, 5, 0, 2},
  {95, 3, 1, 0, 3},
  {106, 3, 2, 0, 3},
  {115, 3, 3, 0, 4},
  {121, 3, 4, 0, 4},
  {142, 3, 5, 0, 4},
  {162, 3, 6, 0, 5},
  {175, 3, 7, 0, 5},
  {189, 2, 1, 0, 6},
  {216, 2, 2, 0, 7},
  {227, 2, 3, 0, 7},
  {240, 2, 4, 0, 7},
  {282, 2, 10, 0, 9},
  {319, 1, 15, 0, 10},
  {347, 2, 7, 0, 11},
  {386, 2, 11, 0, 12},
  {427, 2, 12, 0, 13},
  {477, 2, 13, 0, 15},
  {512, 2, 14, 0, 16},
  {562, 1, 5, 0, 17},
  {641, 1, 6, 0, 20},
  {692, 1, 7, 0, 22},
  {752, 0, 1, 0, 23},
  {832, 0, 2, 0, 26},
  {900, 0, 3, 0, 28},
  {953, 0, 4, 0, 30},
  {1121, 0, 5, 0, 35},
  {1279, 0, 6, 0, 40},
  {1379, 0, 7, 0, 43},
  {1387, 0, 10, 0, 43},
  {1542, 0, 11, 0, 48},
  {1680, 1, 8, 1, 52},
  {1811, 1, 9, 1, 56},
  {2084, 1, 10, 1, 65},
  {2320, 1, 11, 1, 72},
  {2560, 1, 12, 1, 80},
  {2693, 0, 3, 1, 84},
  {2849, 0, 4, 1, 89},
  {3355, 0, 8, 1, 104},
  {3618, 0, 9, 1, 113},
  {4202, 0, 10, 1, 131},
  {4684, 0, 11, 1, 146},
  {5188, 0, 12, 1, 161},
  {5770, 0, 13, 1, 180},
  {6207, 0, 14, 1, 193},
#endif
};

static const _RF22BWTableRow _rf22BWTableH10[] = { /* Modulation Index H>=10 */
#if RF22_USE_BW_TABLES
  {26, 5, 1, 0, 1},
  {28, 5, 2, 0, 1},
  {31, 5, 3, 0, 1},
  {32, 5, 4, 0, 1},
  {37, 5, 5, 0, 1},
  {49, 4, 1, 0, 2},
  {54, 4, 2, 0, 2},
  {59, 4, 3, 0, 2},
  {61, 4, 4, 0, 2},
  {72, 4, 5, 0, 2},
  {95, 3, 1, 0, 3},
  {106, 3, 2, 0, 4},
  {115, 3, 3, 0, 4},
  {121, 3, 4, 0, 4},
  {142, 3, 5, 0, 5},
  {162, 3, 6, 0, 6},
  {175, 3, 10, 0, 6},
  {194, 3, 11, 0, 7},
  {214, 3, 12, 0, 8},
  {239, 3, 13, 0, 8},
  {257, 3, 14, 0, 9},
  {282, 2, 5, 0, 10},
  {322, 2, 6, 0, 11},
  {347, 2, 7, 0, 12},
  {386, 2, 11, 0, 14},
  {427, 2, 12, 0, 15},
  {477, 2, 13, 0, 15},
  {512, 2, 14, 0, 16},
  {562, 1, 5, 0, 17},
  {641, 1, 6, 0, 20},
  {694, 1, 10, 0, 22},
  {771, 1, 11, 0, 24},
  {851, 1, 12, 0, 26},
  {953, 1, 13, 0, 30},
  {1022, 1, 14, 0, 32},
  {1156, 2, 11, 1, 36},
  {1277, 2, 12, 1, 40},
  {1428, 2, 13, 1, 44},
  {1533, 2, 14, 1, 48},
  {1680, 1, 8, 1, 52},
  {1811, 1, 9, 1, 56},
  {2084, 1, 10, 1, 65},
  {2320, 1, 11, 1, 72},
  {2560, 1, 12, 1, 80},
  {2693, 0, 3, 1, 84},
  {2849, 0, 4, 1, 89},
  {3355, 0, 8, 1, 104},
  {3618, 0, 9, 1, 113},
  {4202, 0, 10, 1, 131},
  {4684, 0, 11, 1, 146},
  {5188, 0, 12, 1, 161},
  {5770, 0, 13, 1, 180},
  {6207, 0, 14, 1, 193},
#endif
};

static void _rf22PutIfFilterBandWidth(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  uint8_t i;
  uint16_t bw_val = ((config->modem.fsk.frequencyDeviation << 1) + config->modem.dataRate) / 100;

  chDbgAssert(bw_val < 6400,
              "_rf22PutIfFilterBandWidth #bandWidth", "out of range");

#if RF22_IF_FILTER_BW == 1
  static const uint16_t bw_tab[] = { /* x100 Hz */
    26, 28, 31, 32, 37, 42, 45, 49, 54, 59, 61, 72, 82, 88, 95, 106, 115, 121, 142, 162, 175, 189, 210, 227, 240, 282, 322, 347, 377, 417, 452, 479, 562, 641, 692, 752, 832, 900, 953, 1121, 1279, 1379, 1428, 1678, 1811, 1915, 2251, 2488, 2693, 2849, 3355, 3618, 4202, 4684, 5188, 5770, 6207,
  };
  
  /* lookup bw table */
  for(i = 0; i < sizeof(bw_tab)/sizeof(bw_tab[0]) && bw_val > bw_tab[i]; i++);

  uint8_t dwn3_bypass =
    i < 42 ? 0 : 1;
  
  uint8_t ndec_exp =
    i < 42 ? (-i + 42 - 1) / 7 :
    i < 45 ? 1 : 0;

  uint8_t filset =
    i < 42 ? (i % 7) + 1 :
    i < 44 ? i - 42 + 4 :
    i < 45 ? 9 : 
    i < 46 ? 15 :
    i < 50 ? i - 46 + 1 : i - 50 + 8;
#endif

  uint32_t data_rate_coef = config->modem.manchester.enable ? (config->modem.dataRate << 1) : config->modem.dataRate;

#if RF22_IF_FILTER_BW == 2
  uint8_t H = (config->modem.fsk.frequencyDeviation << 1)/data_rate_coef;
  
  const _RF22BWTableRow *bw_tab;
  uint8_t bw_len;
  
  if(H < 2){
    bw_tab = _rf22BWTableH1;
    bw_len = sizeof(_rf22BWTableH1)/sizeof(_RF22BWTableRow);
  }else if(H < 10){
    bw_tab = _rf22BWTableH2;
    bw_len = sizeof(_rf22BWTableH2)/sizeof(_RF22BWTableRow);
  }else{
    bw_tab = _rf22BWTableH10;
    bw_len = sizeof(_rf22BWTableH10)/sizeof(_RF22BWTableRow);
  }

  /* lookup bw table */
  for(i = 0; i < bw_len && bw_val > bw_tab[i].band_width; i++);
  if(i == bw_len) i--;
  
  uint8_t ndec_exp = bw_tab[i].ndec_exp;
  uint8_t filset = bw_tab[i].filset;
  uint8_t dwn3_bypass = bw_tab[i].dwn3_bypass;

#endif
  
  uint32_t dwn3_bypass_coef = dwn3_bypass ? 125 * 3 * 1000 : 125 * 1000;

  /*
    Math (simplified by maxima):
    
    Q[rxosr]: rxosr=500*(1+2*dwn3_bypass)/(2^(ndec_exp-3)*Rb*(1+enmanch));
    Q[ncoff]: ncoff=Rb*(1+enmanch)*2^(20+ndec_exp)/(500*(1+2*dwn3_bypass));
    Q[crgain]: crgain=2+(2^16*Rb*(1+enmanch))/(rxosr*Fd), Q[rxosr];
    
    Ex:
    
    P: [Fd=50, Rb=100, enmanch=0, ndec_exp=0, dwn3_bypass=1, filset=1];
    Q[rxosr], P;
    Q[ncoff], P, numer;
    Q[crgain], P, numer;
  */
  
  uint16_t rxosr = (dwn3_bypass_coef << (5 - ndec_exp)) / data_rate_coef;
  
#if 0
  /* less precision */
  uint32_t ncoff = (data_rate_coef << (ndec_exp + 8)) / (dwn3_bypass_coef >> 10);
#else
  /* high precision */
  uint32_t ncoff = ((uint64_t)data_rate_coef << (ndec_exp + 18)) / dwn3_bypass_coef;
#endif

  /* Hmm...
  uint8_t crgain_factor = config->modem.dataRate == 100000 ? 2 : 1;
  
  uint32_t crgain = 2 + crgain_factor * (data_rate_coef << 16) / (rxosr * config->modem.fsk.frequencyDeviation);
  crgain = crgain > 2047 ? 2047 : crgain;
  */
  
#if 0
  /* less precision */
  uint32_t crgain = 2 + (data_rate_coef << 13) / (rxosr * config->modem.fsk.frequencyDeviation >> 3);
#else
  /* high precision */
  uint32_t crgain = 2 + ((uint64_t)data_rate_coef << 16) / (rxosr * config->modem.fsk.frequencyDeviation);
#endif
  
  uint8_t crgain_factor = crgain > 0x7ff ? 2 : 1;
  
  if(crgain_factor == 2) crgain >>= 1;
  if(crgain > 0x7ff) crgain = 0x7ff;
  
  uint8_t bw[] = {
    /* RF22_REG_IF_FILTER_BANDWIDTH 1c */
    ((ndec_exp <<
      RF22_IF_FILTER_DECIMATION_RATES_SHIFT) &
     RF22_IF_FILTER_DECIMATION_RATES_MASK) |
    
    (dwn3_bypass ? RF22_BYPASS_DECIMATE_BY_3_STAGE : 0) |
    
    ((filset <<
      RF22_IF_FILTER_COEFFICIENT_SETS_SHIFT) &
     RF22_IF_FILTER_COEFFICIENT_SETS_MASK),
    
    /* RF22_REG_AFC_LOOP_GEARSHIFT_OVERRIDE 1d */
    RF22_AFC_1P5DB_OR_0DB |
    (config->modem.fsk.afcEnable ? RF22_AFC_ENABLE :
     (RF22_AFC_HIGH_GEAR_SETTING_MASK <<
      RF22_AFC_HIGH_GEAR_SETTING_SHIFT)),
    
    /* RF22_REG_AFC_TIMING_CONTROL 1e */
    config->modem.fsk.afcEnable && (data_rate_coef < 200000) ?
    
    ((0 << RF22_SWAIT_TIMER_OFFSET) & RF22_SWAIT_TIMER_MASK) |
    ((1 << RF22_SHWAIT_OFFSET) & RF22_SHWAIT_MASK) |
    ((2 << RF22_ANWAIT_OFFSET) & RF22_ANWAIT_MASK) :
    
    ((0 << RF22_SWAIT_TIMER_OFFSET) & RF22_SWAIT_TIMER_MASK) |
    ((0 << RF22_SHWAIT_OFFSET) & RF22_SHWAIT_MASK) |
    ((2 << RF22_ANWAIT_OFFSET) & RF22_ANWAIT_MASK),
    
    /* RF22_REG_CLOCK_RECOVERY_GEARSHIFT_OVERRIDE 1f */
    /* if Rb offset < 1% then 3 else 0 */
    ((0 <<
      RF22_CLOCK_RECOVERY_FAST_GEARSHIFT_SHIFT) &
     RF22_CLOCK_RECOVERY_FAST_GEARSHIFT_MASK) |
    ((3 <<
      RF22_CLOCK_RECOVERY_SLOW_GEARSHIFT_SHIFT) &
     RF22_CLOCK_RECOVERY_SLOW_GEARSHIFT_MASK),

    /* RF22_REG_CLOCK_RECOVERY_OVERSAMPLING_RATE 20 */
    rxosr & 0xff,
    
    /* RF22_REG_CLOCK_RECOVERY_OFFSET2 21 */
    (((rxosr & 0xff00) >> 8 <<
      RF22_OVERSAMPLING_RATE_MSB_SHIFT) &
     RF22_OVERSAMPLING_RATE_MSB_MASK) |
    
    ((ncoff >> 16 <<
      RF22_NCO_OFFSET_MSB_SHIFT) &
     RF22_NCO_OFFSET_MSB_MASK),
    
    /* RF22_REG_CLOCK_RECOVERY_OFFSET1 22 */
    (ncoff >> 8) & 0xff,

    /* RF22_REG_CLOCK_RECOVERY_OFFSET0 23 */
    ncoff & 0xff,

    /* RF22_REG_CLOCK_RECOVERY_TIMING_LOOP_GAIN1 24 */
    ((crgain >> 8 <<
      RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_MSB_SHIFT) &
     RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_MSB_MASK) |
    
    (crgain_factor == 2 ? RF22_CLOCK_RECOVERY_TIMING_LOOP_GAIN_2X : 0),

    /* RF22_REG_CLOCK_RECOVERY_TIMING_LOOP_GAIN0 25 */
    crgain & 0xff,
  };
  
  _rf22RegPut(radio, RF22_REG_IF_FILTER_BANDWIDTH, sizeof(bw), bw);
  
  /*
    Algo from spreadsheet. FIXME!
    
    txrx_crystal_tolerance = (tx_crystal_tolerance + rx_crystal_tolerance) / 2; // ppm

    txrx_extreme = 4 * txrx_crystal_tolerance * (carrier_frequency / 1000000) / 1000; // KHz

    freqLim = txrx_extreme / 2 + 0.5;

    afcLim1 = 1000 * afcLimAdj / (4 * (hbsel + 1) * 156.25);

    afcLim2 = 1000 * freqLim / (4 * (hbsel + 1) * 156.25) - (txrx_crystal_tolerance > 25 && mod_index > 1 ? 1 : 0);
    
    afcLim = (bw_val / 10) >= txrx_extreme ? (afcLim1) : (afcLim2);
    
    afcLimiter = afcLim > 80 ? 80 : afcLim; // hmm…
  */
  
  if(config->modem.fsk.afcEnable){
    uint8_t afc_lim =
#if RF22_IF_FILTER_BW == 2
      (config->modem.fsk.afcPullInRange == RF22AfcPullInRangeDefault) ?
      afc_lim = bw_tab[i].afc_limiter :
#endif
      ((config->modem.fsk.afcPullInRange >> (config->carrierFrequency >= 480000000 ? 1 : 0)) / 625);
    
    _rf22RegPut(radio, RF22_REG_AFC_LIMITER, 1, &afc_lim);
  }
}

static void _rf22PutAGCOverride(RF22Driver *radio){
  uint8_t ao[] = {
    /* RF22_REG_AGC_OVERRIDE1 */
    RF22_AGC_GAIN_INC_DURING_SIGNAL_REDUCTS |
    RF22_AGC_ENABLE
  };
  
  _rf22RegPut(radio, RF22_REG_AGC_OVERRIDE1, sizeof(ao), ao);
}

static void _rf22PutModulation(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  _rf22PutAGCOverride(radio);
  
  uint8_t dr2_mc2_fd1[] = {
    0, /* RF22_REG_TX_DATA_RATE1 0x6e */
    0, /* RF22_REG_TX_DATA_RATE0 0x6f */
    0, /* RF22_REG_MODULATION_CONTROL1 0x70 */
    0, /* RF22_REG_MODULATION_CONTROL1 0x71 */
    0  /* RF22_REG_FREQUENCY_DEVIATION 0x72 */
  };
  
  { /* setup data rate */
    chDbgAssert(config->modem.dataRate >= 123 &&
                config->modem.dataRate <= 256000,
                "_rf22PutModulation #dataRate", "out of range");
    
    bool_t txdrscale = config->modem.dataRate < 30000 ? TRUE : FALSE;
    
    /* txdr = Rb×1000 * 2 ^ (txdrscale ? 21 : 16) / 10 ^ 6 */
    /* txdr = Rb×1000 << (txdrscale ? 15 : 10) / (10 ^ 6 >> 6) */
    /* txdr = Rb×1000 << (txdrscale ? 15 : 10) / (10 ^ 6 >> 7) >> 1 (simulate rounding) */
    
    uint16_t txdr = (uint32_t)(config->modem.dataRate << (txdrscale ? 15 : 10)) / (1000000 >> 7) >> 1;
    
    dr2_mc2_fd1[0] = txdr >> 8;
    dr2_mc2_fd1[1] = txdr & 0xff;
    
    dr2_mc2_fd1[2] |= txdrscale ? RF22_TX_DATA_RATE_BELOW_30KBPS : 0;
  }
  
  { /* setup charge pump current trimming */
    /* Rb_eff = enmanch ? Rb×1000 * 2 : Rb; cpcuu = Rb_eff < 100×1000 ? 0x80 : < 200×1000 ? 0xc0 : 0xed */
    /* Rb_eff = Rb×1000 >> (enmanch ? 4 : 5); cpcuu = Rb_eff < (100000 >> 5) ? 0x80 : < (200000 >> 5) ? 0xc0 : 0xed */
    
    uint16_t dr_eff = config->modem.dataRate >> (config->modem.manchester.enable ? 4 : 5);
    uint8_t cpcuu = dr_eff < (100000 >> 5) ? 0x80 : dr_eff < (200000 >> 5) ? 0xc0 : 0xed;
    
    _rf22RegPut(radio, RF22_REG_CHARGE_PUMP_CURRENT_TRIMMING, 1, &cpcuu);
  }
  
  switch(config->modem.type){
  case RF22ModulationOOK:
    
    break;
  case RF22ModulationFSK:
  case RF22ModulationGFSK:
    { /* setup frequency deviation value */
      chDbgAssert(config->modem.fsk.frequencyDeviation >= 625 &&
                  config->modem.fsk.frequencyDeviation <= 320000,
                  "_rf22PutModulation #frequencyDeviation", "out of range");
      
      uint16_t fd = config->modem.fsk.frequencyDeviation / 625;
      
      dr2_mc2_fd1[3] |= fd & 0x100;
      dr2_mc2_fd1[4] = fd & 0xff;
    }
    
    break;
  default:
    break;
  }
  
  dr2_mc2_fd1[2] |=
    (config->modem.manchester.enable ?
     RF22_ENABLE_MANCHESTER_ENCODING : 0) |
    
    (config->modem.manchester.inversion ?
     RF22_ENABLE_MANCHESTER_DATA_INVERSION : 0) |

    (config->modem.manchester.preamblePolarity ?
     RF22_MANCHESTER_PREAMBLE_POLARITY : 0) |
    
    (config->modem.whitening ?
     RF22_ENABLE_DATA_WHITENING : 0);
  
  dr2_mc2_fd1[3] |=
    ((config->modem.type <<
      RF22_MODULATION_TYPE_SHIFT) &
     RF22_MODULATION_TYPE_MASK) |
    
    ((config->modem.dataSource <<
      RF22_DATA_SOURCE_SHIFT) &
     RF22_DATA_SOURCE_MASK) |
  
    (config->modem.dataInversion ?
     RF22_ENABLE_DATA_INVERSION : 0) |
    
    ((config->modem.dataClock <<
      RF22_TX_DATA_CLOCK_SHIFT) &
     RF22_TX_DATA_CLOCK_MASK);
  
  _rf22RegPut(radio, RF22_REG_TX_DATA_RATE1, sizeof(dr2_mc2_fd1), dr2_mc2_fd1);

  switch(config->modem.type){
  case RF22ModulationOOK:
    
    break;
  case RF22ModulationFSK:
  case RF22ModulationGFSK:
    _rf22PutIfFilterBandWidth(radio);
    break;
  default:
    break;
  }
}

#endif/*RF22_USE_PREDEF_MODEM_CONFIG*/

/*
 * Amplification setting
 */

static void _rf22PutPower(RF22Driver *radio){
  chDbgAssert(radio->txPower < 64,
              "_rf22PutPower #txPower", "out of range");
  
  _rf22RegPut1(radio, RF22_REG_TX_POWER, radio->txPower | RF22_LNA_SW);
}

void rf22SetPower(RF22Driver *radio, uint8_t power){
  chDbgCheck(radio != NULL, "rf22SetPower");

  chSysLock();
  
  chDbgAssert(radio->state != RF22_STOP,
              "rf22SetPower", "invalid state");
  
  radio->txPower = power;

  chSysUnlock();
  
  if(radio->state != RF22_ERROR){
    _rf22AcquireSpi(radio);
    
    _rf22PutPower(radio);
    
    _rf22ReleaseSpi(radio);
  }
}

/*
 * Transport setting
 */

static void _rf22PutFifoControl(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  chDbgAssert(config->fifoControl.txAlmostFull < 64,
              "_rf22PutFifoControl #txAlmostFull", "out of range");

  chDbgAssert(config->fifoControl.txAlmostEmpty < 64,
              "_rf22PutFifoControl #txAlmostEmpty", "out of range");
  
  chDbgAssert(config->fifoControl.rxAlmostFull < 64,
              "_rf22PutFifoControl #rxAlmostFull", "out of range");
  
  uint8_t ths[] = {
    (config->fifoControl.txAlmostFull > 0 ?
     config->fifoControl.txAlmostFull - 1 : 0),
    (config->fifoControl.txAlmostEmpty > 0 ?
     config->fifoControl.txAlmostEmpty - 1 : 0),
    (config->fifoControl.rxAlmostFull > 0 ?
     config->fifoControl.rxAlmostFull - 1 : 0),
  };
  
  _rf22RegPut(radio, RF22_REG_TX_FIFO_CONTROL1, sizeof(ths), ths);
}

static inline uint8_t _rf22HeaderCheck(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  return
    ((
      (radio->broadcast /*&&
       (config->packet.header.check.mask == 0)*/ ?
       0xff : 0x00) <<
      RF22_BROADCAST_ADDR_CHECK_SHIFT) &
     RF22_BROADCAST_ADDR_CHECK_MASK) |
    
    (((
       (config->packet.header.length > 0 &&
        ((config->packet.header.check.mask >> 0) & 0xff) ?
        (1 << 3) : 0)
       |
       (config->packet.header.length > 1 &&
        ((config->packet.header.check.mask >> 8) & 0xff) ?
        (1 << 2) : 0)
       |
       (config->packet.header.length > 2 &&
        ((config->packet.header.check.mask >> 16) & 0xff) ?
        (1 << 1) : 0)
       |
       (config->packet.header.length > 3 &&
        ((config->packet.header.check.mask >> 24) & 0xff) ?
        (1 << 0) : 0)
       ) <<
      RF22_RECEIVED_BYTES_TO_CHECK_SHIFT) &
     RF22_RECEIVED_BYTES_TO_CHECK_MASK);
}

static void _rf22PutPacketHandling(RF22Driver *radio){
  const RF22Config *config = radio->config;
  
  chDbgAssert(config->packet.preamble.length <= 2040,
              "_rf22PutPacketHandling #preambleLength", "out of range");

  chDbgAssert(config->packet.preamble.threshold <= 504,
              "_rf22PutPacketHandling #preambleThreshold", "out of range");

  chDbgAssert(config->packet.syncWord.length > 0 &&
              config->packet.syncWord.length <= 4,
              "_rf22PutPacketHandling #syncWordLength", "out of range");
  
  chDbgAssert(config->packet.header.length <= 4,
              "_rf22PutPacketHandling #headerLength", "out of range");
  
  uint8_t ac =
    (config->packet.handleTx ? RF22_ENABLE_PACKET_TX_HANDLING : 0) |
    (config->packet.handleRx ? RF22_ENABLE_PACKET_RX_HANDLING : 0) |
    (config->packet.bitsOrder == RF22BitsOrderLSB ? RF22_LSB_FIRST_ENBALE : 0) |
    (config->packet.preamble.skip2Phase ? RF22_SKIP_2ND_PHASE_PREAMBLE_DETECTION : 0) |
    (config->packet.crc.option == RF22CrcNone ? 0 : RF22_ENABLE_CRC) |
    (config->packet.crc.region == RF22CrcDataOnly ? RF22_CRC_DATA_ONLY_ENABLE : 0) |
    (((config->packet.crc.option -
       RF22CrcCCIT + RF22_CRC_CCITT) <<
      RF22_CRC_POLYNOMIAL_SELECTION_SHIFT) &
     RF22_CRC_POLYNOMIAL_SELECTION_MASK);
  
  _rf22RegPut(radio, RF22_REG_DATA_ACCESS_CONTROL, 1, &ac);
  
  uint8_t ph[] = {
    /* RF22_REG_HEADER_CONTROL1 */
    _rf22HeaderCheck(radio),
    
    /* RF22_REG_HEADER_CONTROL2 */
    (config->packet.preamble.length > 1020 ?
     RF22_MSB_OF_PREAMBLE_LENGTH : 0) |
    
    (config->packet.syncWord.skipSearchTimeout ?
     RF22_SKIP_SYNC_WORD_SEARCH_TIMEOUT : 0) |
    
    (config->packet.dataLength != RF22DataLengthVariable ?
     RF22_FIX_PACKET_LENGTH : 0) |
    
    (((config->packet.syncWord.length - 1) <<
      RF22_SYNC_WORD_LENGTH_SHIFT) &
     RF22_SYNC_WORD_LENGTH_MASK) |
    
    (((config->packet.header.length) <<
      RF22_HEADER_LENGTH_SHIFT) &
     RF22_HEADER_LENGTH_MASK),
    
    /* RF22_REG_PREAMBLE_LENGTH */
    config->packet.preamble.length >> 2,
    
    /* RF22_REG_PREAMBLE_DETECTION_CONTROL1 */
    (((config->packet.preamble.threshold >> 2) <<
      RF22_PREAMBLE_DETECTION_THRESHOLD_SHIFT) &
     RF22_PREAMBLE_DETECTION_THRESHOLD_MASK),
  };
  
  _rf22RegPut(radio, RF22_REG_HEADER_CONTROL1, sizeof(ph), ph);
  
  _rf22RegPutL(radio, RF22_REG_SYNC_WORD3,
               config->packet.syncWord.length,
               config->packet.syncWord.value);
  
  /* packet length for fixed-size packets */
  if(config->packet.dataLength != RF22DataLengthVariable){
    _rf22RegPut(radio, RF22_REG_TRANSMIT_PACKET_LENGTH, 1,
                &config->packet.dataLength);
  }

  if(config->packet.header.length > 0){
    _rf22RegPutL(radio, RF22_REG_CHECK_HEADER3,
                 config->packet.header.length,
                 radio->header.check);
    
    _rf22RegPutL(radio, RF22_REG_HEADER_ENABLE3,
                 config->packet.header.length,
                 config->packet.header.check.mask);

    _rf22RegPutL(radio, RF22_REG_TRANSMIT_HEADER3,
                 config->packet.header.length,
                 radio->header.send);
  }
}

/*
 * Data handling
 */

static void _rf22PutGPIO(RF22Driver *radio){
  const RF22GpioConfig *config = radio->config->gpioConfig;
  
  uint8_t gpio[] = {
    ((config[0].driving <<
      RF22_GPIO_DRIVING_CAPABILITY_SHIFT) &
     RF22_GPIO_DRIVING_CAPABILITY_MASK) |

    (config[0].pullup ? RF22_GPIO_ENABLE_200K_PULLUP_RESISTOR : 0) |
    
    ((config[0].option <<
      RF22_GPIO_FUNCTION_SELECT_SHIFT) &
     RF22_GPIO_FUNCTION_SELECT_MASK),
    
    ((config[1].driving <<
      RF22_GPIO_DRIVING_CAPABILITY_SHIFT) &
     RF22_GPIO_DRIVING_CAPABILITY_MASK) |

    (config[1].pullup ? RF22_GPIO_ENABLE_200K_PULLUP_RESISTOR : 0) |
    
    ((config[1].option <<
      RF22_GPIO_FUNCTION_SELECT_SHIFT) &
     RF22_GPIO_FUNCTION_SELECT_MASK),

    ((config[2].driving <<
      RF22_GPIO_DRIVING_CAPABILITY_SHIFT) &
     RF22_GPIO_DRIVING_CAPABILITY_MASK) |
    
    (config[2].pullup ? RF22_GPIO_ENABLE_200K_PULLUP_RESISTOR : 0) |
    
    ((config[2].option <<
      RF22_GPIO_FUNCTION_SELECT_SHIFT) &
     RF22_GPIO_FUNCTION_SELECT_MASK),
  };
  
  _rf22RegPut(radio, RF22_REG_GPIO_CONFIGURATION0, sizeof(gpio), gpio);
}

static void _rf22RegsReset(RF22Driver *radio){
  _rf22RegPut1(radio, RF22_REG_OPERATING_MODE1, RF22_OP1_SOFTWARE_REGISTER_RESET);
  /* reseting registers take some time */
  chThdSleepMicroseconds(100);
}

static void _rf22FifoReset(RF22Driver *radio, uint8_t reset){
  uint8_t om2;
  
  _rf22RegGet(radio, RF22_REG_OPERATING_MODE2, 1, &om2);
  
  om2 |= reset;
  
  _rf22RegPut(radio, RF22_REG_OPERATING_MODE2, 1, &om2);
  
  om2 &= ~reset;
  
  _rf22RegPut(radio, RF22_REG_OPERATING_MODE2, 1, &om2);
}

static bool_t _rf22CheckDevice(RF22Driver *radio){
  uint8_t di[2];
  
  _rf22RegGet(radio, RF22_REG_DEVICE_TYPE, sizeof(di), di);
  
  return
    (di[0] == RF22_DEVICE_TYPE_RX_TRX ||
     di[0] == RF22_DEVICE_TYPE_TX) &&
    (di[1] == RF22_VERSION_CODE_6 ||
     di[1] == RF22_VERSION_CODE_7);
}

static void _rf22PutBroadcast(RF22Driver *radio){
  uint8_t hc1 = _rf22HeaderCheck(radio);
  
  _rf22RegPut(radio, RF22_REG_HEADER_CONTROL1, 1, &hc1);
}

void rf22SetBroadcast(RF22Driver *radio, bool_t state){
  const RF22Config *config = radio->config;
  
  chDbgCheck(radio != NULL && config != NULL, "rf22SetBroadcast");

  chSysLock();

  chDbgAssert(radio->state != RF22_STOP, "rf22SetBroadcast()", "invalid state");
  
  radio->broadcast = state;
  
  chSysUnlock();
  
  if(radio->state != RF22_ERROR){
    _rf22PutBroadcast(radio);
  }
}

static void _rf22PutSettings(RF22Driver *radio){
  _rf22PutFrequency(radio);
  _rf22PutChannelStep(radio);
  _rf22PutChannel(radio);
  
  _rf22PutModulation(radio);
  
  _rf22PutPower(radio);
  _rf22PutGPIO(radio);
  
  _rf22PutFifoControl(radio);
  _rf22PutPacketHandling(radio);
  
  _rf22PutBroadcast(radio);
}

static void _rf22LedState(const RF22LedConfig *led, bool_t on){
  if((led->mode == RF22LedOnLo && on) || (led->mode == RF22LedOnHi && !on)){
    palClearPad(led->port, led->pad);
  }
  if((led->mode == RF22LedOnLo && !on) || (led->mode == RF22LedOnHi && on)){
    palSetPad(led->port, led->pad);
  }
}

static void _rf22TryInterrupt(RF22Driver *radio){
  if(radio->state == RF22_LISTEN &&
     (radio->thread == NULL ||
      chThdGetPriority() >= radio->thread->p_prio)){
    /* cancel receiving wait operation */
    chSemReset(&radio->eventSemaphore, 0);
  }
}

static void _rf22PowerOff(RF22Driver *radio){
  palSetPad(radio->config->sdn.port, radio->config->sdn.pad);
  radio->state = RF22_STOP;
}

static void _rf22PowerOn(RF22Driver *radio){
  palClearPad(radio->config->sdn.port, radio->config->sdn.pad);
  
  const uint8_t check_interval = 1; /* ms */
  const uint8_t check_timeout = 27; /* ms */
  uint8_t i;
  
  /* check device type and version instead of chThdSleepMilliseconds(17) */
  for(i = 0; i < check_timeout; i += check_interval){
    if(_rf22CheckDevice(radio)){
      /* put radio settings */
      _rf22PutSettings(radio);
      
      radio->state = RF22_READY;
      
      return;
    }
    
    chThdSleepMilliseconds(check_interval);
  }
  
  radio->state = RF22_ERROR;
}

static void _rf22PutState(RF22Driver *radio){
  uint8_t st, os1;
  
  /* get current hardware status */
  _rf22GetStatus(radio, &st);

  st &= RF22_CHIP_POWER_STATE_MASK;

  if((st == RF22_CHIP_IDLE && radio->state == RF22_READY) ||
     (st == RF22_CHIP_RX && (radio->state == RF22_LISTEN ||
                             radio->state == RF22_RECEIVE)) ||
     (st == RF22_CHIP_TX && radio->state == RF22_TRANSMIT)){
    /* already in desired state */
    return;
  }

  _rf22RegGet(radio, RF22_REG_OPERATING_MODE1, 1, &os1);
  
  os1 &= ~(RF22_OP1_READY_MODE_CRYSTAL_ON | RF22_OP1_MANUAL_TRANSMIT_MODE_TX_ON | RF22_OP1_MANUAL_RECEIVER_MODE_RX_ON);
  
  switch(radio->state){
  case RF22_READY:
    os1 |= RF22_OP1_READY_MODE_CRYSTAL_ON;
    break;
  case RF22_LISTEN:
    os1 |= RF22_OP1_READY_MODE_CRYSTAL_ON | RF22_OP1_MANUAL_RECEIVER_MODE_RX_ON;
    break;
  case RF22_TRANSMIT:
    os1 |= RF22_OP1_READY_MODE_CRYSTAL_ON | RF22_OP1_MANUAL_TRANSMIT_MODE_TX_ON;
    break;
  default:
    break;
  }
  
  _rf22RegPut(radio, RF22_REG_OPERATING_MODE1, 1, &os1);
}

static void _rf22SetState(RF22Driver *radio, RF22State state){
  if(state == RF22_STOP && radio->state != RF22_STOP){
    _rf22PowerOff(radio);
    return;
  }
  
  if(radio->state == RF22_STOP && state != RF22_STOP){
    _rf22PowerOn(radio);
  }
  
  if(radio->state == RF22_ERROR){
    return;
  }
  
  radio->state = state;
  
  _rf22PutState(radio);
}

void rf22ObjectInit(RF22Driver *radio){
  chDbgCheck(radio != NULL, "rf22ObjectInit");
  
  radio->config = NULL;
  radio->state = RF22_STOP;
  
  radio->thread = NULL;
  
  chSemInit(&radio->eventSemaphore, 0);
  
#if RF22_USE_MUTUAL_EXCLUSION
# if CH_USE_MUTEXES
  chMtxInit(&radio->mutex);
# else
  chSemInit(&radio->semaphore, 1);
# endif
#endif/* RF22_USE_MUTUAL_EXCLUSION */
}

void rf22Start(RF22Driver *radio, const RF22Config *config){
  chDbgCheck(radio != NULL && config != NULL && config->spip != NULL && config->spicfg != NULL, "rf22Start");
  
  chSysLock();
  
  chDbgAssert(radio->state == RF22_STOP,
              "rf22Start()", "invalid state");
  
  radio->config = config;
  
  /* setup defaults */
  radio->hoppingChannel = config->hoppingChannel;
  radio->txPower = config->txPower;
  
  /* setup default headers */
  radio->header.check = config->packet.header.check.value;
  radio->header.send = config->packet.header.send.value;
  
  radio->broadcast = config->packet.header.check.broadcast;
  
  chSysUnlock();

  _rf22AcquireIface(radio);
  
  _rf22SetState(radio, RF22_READY);
  
  _rf22ReleaseIface(radio);
}

void rf22Stop(RF22Driver *radio){
  chDbgCheck(radio != NULL, "rf22Stop");

  chDbgAssert(radio->state != RF22_STOP,
              "rf22Stop()", "invalid state");
  
  _rf22AcquireIface(radio);
  
  chSemReset(&radio->eventSemaphore, 0);
  
  _rf22SetState(radio, RF22_STOP);
  
  _rf22ReleaseIface(radio);
  
  radio->config = NULL;
}

/*
 * Data handling
 */

#define min(a, b) ((a)<(b)?(a):(b))

static void _rf22PutTxHeader(RF22Driver *radio, RF22Header value){
  const RF22Config *config = radio->config;

  bool_t change = FALSE;
  
  if(value == 0){
    if(config->packet.header.send.value != radio->header.send){
      radio->header.send = config->packet.header.send.value;
      change = TRUE;
    }
  }else{
    if(value != radio->header.send){
      radio->header.send = value;
      change = TRUE;
    }
  }

  if(change){
    _rf22RegPutL(radio, RF22_REG_TRANSMIT_HEADER3,
                 config->packet.header.length, value);
  }
}

static void _rf22PutRxHeader(RF22Driver *radio, RF22Header value){
  const RF22Config *config = radio->config;
  
  bool_t change = FALSE;

  if(value == 0){
    if(config->packet.header.check.value != radio->header.check){
      radio->header.check = config->packet.header.check.value;
      change = TRUE;
    }
  }else{
    if(value != radio->header.check){
      radio->header.check = value;
      change = TRUE;
    }
  }
  
  if(change){
    _rf22RegPutL(radio, RF22_REG_CHECK_HEADER3,
                 config->packet.header.length, value);
  }
}

static const uint8_t _rf22SendEventsMask[] = {
#ifdef RF22_DEBUG
  RF22_EN1_FIFO_UNDERFLOW_OVERFLOW |
#endif
  RF22_EN1_TX_FIFO_ALMOST_EMPTY |
  RF22_EN1_PACKET_SENT,
  
  RF22_EN2_NONE,
};

static RF22Status _rf22PacketSend(RF22Driver *radio, RF22Length len, const RF22Buffer *buf){
  const RF22Config *config = radio->config;
  
  RF22Status status = RF22Success;
  uint8_t size;
  uint8_t deviceStatus;
  uint8_t polled[2];
  
  dbgPrint("prepare send %u bytes", len);
  
  /* calculate the data piece size to write to tx fifo */
  size = min(RF22_TX_FIFO_LENGTH, len);
  
  _rf22FifoReset(radio, RF22_OP2_TX_FIFO_RESET_CLEAR | RF22_OP2_RX_FIFO_RESET_CLEAR);
  
  if(config->packet.dataLength == RF22DataLengthVariable){
    /* setup packet length to send */
    _rf22RegPut(radio, RF22_REG_TRANSMIT_PACKET_LENGTH, 1, &len);
  }
  
  dbgPrint("initial put %u bytes into tx fifo", size);
  
  /* write initial data from buffer to tx fifo */
  _rf22RegPut(radio, RF22_REG_FIFO_ACCESS_DATA, size, buf);
  
  buf += size;
  len -= size;
  
  /* configure events to poll */
  _rf22MaskEvents(radio, _rf22SendEventsMask);
  
  /* initiate packet sending */
  _rf22SetState(radio, RF22_TRANSMIT);
  
  /* tx led on */
  _rf22LedState(&config->led.tx, TRUE);
  
  for(; ; ){
    /* poll sending events */
    if(_rf22PollEvents(radio, polled) != RDY_OK){
      dbgPrint("cancel sending");
      
      status = RF22Cancel;
      break;
    }
    
    _rf22GetStatus(radio, &deviceStatus);
    
#ifdef RF22_DEBUG
    /* invalid transfer state */
    if(polled[0] & RF22_INT1_FIFO_UNDERFLOW_OVERFLOW){
      dbgPrint("fifo error %s", deviceStatus & RF22_FIFO_OVERFLOW ? "(overflow)" : deviceStatus & RF22_FIFO_UNDERFLOW ? "(underflow)" : "");
      
      status = RF22Error;
      break;
    }
#endif
    
    /* sent complete */
    if(polled[0] & RF22_INT1_PACKET_SENT){
      dbgPrint("packet sent");

      if(len > 0){
        dbgPrint("overflow");
      }else{
        dbgPrint("success");
      }
      
      /* len > 0 means that packet has been sent but buffer hasn't yet been empty */
      status = len > 0 ? RF22Overflow : RF22Success;
      break;
    }
    
    /* continue send */
    if(polled[0] & RF22_INT1_TX_FIFO_ALMOST_EMPTY){
      dbgPrint("tx fifo almost empty");
      
      /* calculate the data piece size to write to tx fifo */
      size = min(RF22_TX_FIFO_LENGTH - config->fifoControl.txAlmostEmpty, len);
      
      if(size > 0){
        dbgPrint("continue put %u bytes into tx fifo", size);
        
        /* continue send data from buffer to tx fifo */
        _rf22RegPut(radio, RF22_REG_FIFO_ACCESS_DATA, size, buf);
        
        buf += size;
        len -= size;
      }
    }
  }
  
  /* disable polling events */
  _rf22UnmaskEvents(radio);

  /* reset tx fifo */
  _rf22FifoReset(radio, RF22_OP2_TX_FIFO_RESET_CLEAR);
  
  /* tx led off */
  _rf22LedState(&config->led.tx, FALSE);
  
  /* switch back to idle mode */
  _rf22SetState(radio, RF22_READY);
  
  return status;
}

RF22Status rf22PacketSend(RF22Driver *radio, RF22Length len, const RF22Buffer *buf, RF22Header header){
  const RF22Config *config = radio->config;
  RF22Status status;
  
  chDbgCheck(radio != NULL && config != NULL, "rf22PacketSend");
  
  chDbgAssert(radio->state != RF22_STOP && radio->state != RF22_ERROR,
              "rf22PacketSend()", "invalid state");
  
  chDbgAssert(config->packet.dataLength == RF22DataLengthVariable ||
              config->packet.dataLength == len,
              "rf22PacketSend(), #packetLength", "mismatch");
  
  /* interrupt listener for transmit */
  _rf22TryInterrupt(radio);
  
  _rf22AcquireIface(radio);
  
  _rf22PutTxHeader(radio, header);
  
  status = _rf22PacketSend(radio, len, buf);
  
  _rf22ReleaseIface(radio);

  return status;
}

static const uint8_t _rf22ReceiveEventsMask[] = {
#ifdef RF22_DEBUG
  RF22_EN1_FIFO_UNDERFLOW_OVERFLOW |
#endif
  RF22_EN1_RX_FIFO_ALMOST_FULL |
  RF22_EN1_VALID_PACKET_RECEIVED |
  RF22_EN1_CRC_ERROR,
  
  RF22_EN2_SYNC_WORD_DETECTED,
};

static RF22Status _rf22PacketReceiveTimeout(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, systime_t timeout){
  const RF22Config *config = radio->config;
  
  RF22Status status = RF22Success;
  
  uint8_t end_len = *len;
  uint8_t size;
  uint8_t deviceStatus;
  uint8_t polled[2];
  
  *len = 0;
  
  dbgPrint("prepare receiving up to %u bytes", end_len);
  
  _rf22FifoReset(radio, RF22_OP2_TX_FIFO_RESET_CLEAR | RF22_OP2_RX_FIFO_RESET_CLEAR);
  
  /* configure events to poll */
  _rf22MaskEvents(radio, _rf22ReceiveEventsMask);
  
  /* initiate packet receiving */
  _rf22SetState(radio, RF22_LISTEN);

  /* turn on rx led */
  _rf22LedState(&config->led.rx, TRUE);
  
  for(; ; ){
    /* poll receiving events */
    msg_t result = _rf22PollEventsTimeout(radio, polled, timeout);
    
    if(result != RDY_OK){
      if(result == RDY_RESET){
        dbgPrint("cancel receiving");
      }else{
        dbgPrint("timeout reached");
      }
      
      status = result == RDY_TIMEOUT ? RF22Timeout : RF22Cancel;
      break;
    }
    
    _rf22GetStatus(radio, &deviceStatus);
    
    /* prepare data receiving */
    if(polled[1] & RF22_INT2_SYNC_WORD_DETECTED){
      dbgPrint("sync word detected");
      
      /* update signal streigth */
      uint8_t rssi;
      /* get actual signal streigth value */
      _rf22RegGet(radio, RF22_REG_RSSI, 1, &rssi);
      
      /* convert to dBm */
      radio->rssi = -120 + (rssi >> 1);
      
      dbgPrint("rssi %d dBm", radio->rssi);
    }
    
#ifdef RF22_DEBUG
    /* invalid transfer state */
    if(polled[0] & RF22_INT1_FIFO_UNDERFLOW_OVERFLOW){
      dbgPrint("fifo error %s", deviceStatus & RF22_FIFO_OVERFLOW ? "(overflow)" : deviceStatus & RF22_FIFO_UNDERFLOW ? "(underflow)" : "");
      
      status = RF22Error;
      break;
    }
#endif

    /* receiving failed */
    if(polled[0] & RF22_INT1_CRC_ERROR){
      dbgPrint("crc error");
      
      status = RF22Invalid;
      break;
    }
    
    /* continue receiving */
    if((polled[0] & RF22_INT1_RX_FIFO_ALMOST_FULL) ||
       (polled[0] & RF22_INT1_VALID_PACKET_RECEIVED)){

      /* switch to receive */
      if(*len == 0){
        _rf22SetState(radio, RF22_RECEIVE);
      }
      
      /* receiving complete */
      if(polled[0] & RF22_INT1_VALID_PACKET_RECEIVED){
        dbgPrint("packet received");
        
        /* calculate the length of rest of data in rx buffer */
        if(config->packet.dataLength == RF22DataLengthVariable){
          /* get received packet data length */
          _rf22RegGet(radio, RF22_REG_RECEIVED_PACKET_LENGTH, 1, &size);
        }else{
          /* use static packet data length */
          size = config->packet.dataLength;
        }
        
        dbgPrint("packet length %u bytes", size);
        
        size -= *len;
        
        /* reduce end length to packet length */
        if(end_len > size){
          end_len = size;
        }
      }else{ /* continue receiving */
        dbgPrint("rx fifo almost full");
        
        /* calculate size of data to read */
        size = config->fifoControl.rxAlmostFull;
      }
      
      /* need to read more data then rest of buffer (buffer already full) */
      
      if(size > end_len){
        status = RF22Overflow;
        size = end_len;
      }
      
      if(size > 0){
        dbgPrint("get %u bytes from rx fifo", size);
        
        /* read data from rx fifo to buffer */
        _rf22RegGet(radio, RF22_REG_FIFO_ACCESS_DATA, size, buf);
        
        buf += size;
        *len += size;
        end_len -= size;
      }
      
      if(status == RF22Overflow){
        dbgPrint("overflow");
        
        break;
      }
      
      /* receiving complete */
      if(polled[0] & RF22_INT1_VALID_PACKET_RECEIVED){
        if(end_len > 0){
          dbgPrint("underflow");
        }else{
          dbgPrint("success");
        }
        
        status = end_len > 0 ? RF22Underflow : RF22Success;
        break;
      }
    }
  }
  
  /* disable polling events */
  _rf22UnmaskEvents(radio);
  
  _rf22FifoReset(radio, RF22_OP2_RX_FIFO_RESET_CLEAR);
  
  /* rx led off */
  _rf22LedState(&config->led.rx, FALSE);
  
  /* switch back to idle mode */
  _rf22SetState(radio, RF22_READY);
  
  return status;
}

RF22Status rf22PacketReceiveTimeout(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, RF22Header header, systime_t timeout){
  const RF22Config *config = radio->config;
  RF22Status status;
  
  chDbgCheck(radio != NULL && config != NULL, "rf22PacketReceive");
  
  chDbgAssert(radio->state != RF22_STOP && radio->state != RF22_ERROR,
              "rf22PacketReceive()", "invalid state");
  
  chDbgAssert(config->packet.dataLength == RF22DataLengthVariable ||
              config->packet.dataLength == *len,
              "rf22PacketReceive(), #packetLength", "static mismatch");
  
  _rf22TryInterrupt(radio);
  
  _rf22AcquireIface(radio);
  
  _rf22PutRxHeader(radio, header);
  
  status = _rf22PacketReceiveTimeout(radio, len, buf, timeout);
  
  _rf22ReleaseIface(radio);

  return status;
}

RF22Status rf22PacketSendReceiveTimeout(RF22Driver *radio, RF22Length send_len, const RF22Buffer *send_buf, RF22Header send_header, RF22Length *recv_len, RF22Buffer *recv_buf, RF22Header recv_header, systime_t recv_timeout){
  const RF22Config *config = radio->config;
  RF22Status status;
  
  chDbgCheck(radio != NULL && config != NULL, "rf22PacketSend");
  
  chDbgAssert(radio->state != RF22_STOP && radio->state != RF22_ERROR,
              "rf22PacketSendReceive()", "invalid state");
  
  chDbgAssert(config->packet.dataLength == RF22DataLengthVariable ||
              (config->packet.dataLength == send_len &&
               config->packet.dataLength == *recv_len),
              "rf22PacketSendReceive(), #packetLength", "mismatch");
  
  /* interrupt listener */
  _rf22TryInterrupt(radio);
  
  _rf22AcquireIface(radio);
  
  _rf22PutTxHeader(radio, send_header);
  
  status = _rf22PacketSend(radio, send_len, send_buf);

  if(status == RF22Success){
    _rf22PutRxHeader(radio, recv_header);
    
    status = _rf22PacketReceiveTimeout(radio, recv_len, recv_buf, recv_timeout);
  }
  
  _rf22ReleaseIface(radio);
  
  return status;
}

/*
 * ACK Feature
 */

#if RF22_WITH_ACK

static RF22Status _rf22PacketReceiveAckTimeout(RF22Driver *radio, systime_t timeout){
  return RDY_OK;
}

RF22Status rf22PacketSendWithAckTimeout(RF22Driver *radio, RF22Length len, const RF22Buffer *buf, RF22Header header, systime_t timeout){
  const RF22Config *config = radio->config;
  
  chDbgCheck(radio != NULL && config != NULL, "rf22PacketSend");

  chDbgAssert(config->packet.dataLength == RF22DataLengthVariable ||
              config->packet.dataLength == len,
              "rf22PacketSend #packetLength", "mismatch");
  
  RF22Status status;
  
  int16_t retry;

  _rf22TryInterrupt(radio);

  _rf22AcquireIface(radio);
  
  for(retry = config->maxRetry + 1; retry >= 0; retry--){
    /* send data packet */
    status = _rf22PacketSend(radio, len, buf);
    
    /* error on send packet */
    if(status != RF22Success){
      return status;
    }
    
    /* receive ack packet */
    status = _rf22PacketReceiveAckTimeout(radio, timeout);
    
    switch(status){
    case RF22Success:
    case RF22Cancel:
      return status;
    default:
      break;
    }
  }
  
  _rf22ReleaseIface(radio);
  
  return RF22MaxRetry;
}

static void _rf22PacketSendAck(RF22Driver *radio, bool_t ok){
  
}

RF22Status rf22PacketReceiveWithAskTimeout(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, systime_t timeout){
  
}

#endif/*RF22_WITH_ACK*/
