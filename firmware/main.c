/*
    ChibiOS/RT - Copyright (C) 2006-2013 Giovanni Di Sirio

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

        http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
*/

#include <stdlib.h>
#include <string.h>

#include "ch.h"
#include "hal.h"

#if WITH_TEST
#include "test.h"
#endif

#include "shell.h"
#include "chprintf.h"

#include "usb_local.h"
#include "rf22_local.h"

static EXTConfig extcfg = {
  {
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL},
    {EXT_CH_MODE_DISABLED, NULL}
  }
};

/*===========================================================================*/
/* Command line related.                                                     */
/*===========================================================================*/

#define SHELL_WA_SIZE   THD_WA_SIZE(2048)

static void cmd_mem(BaseSequentialStream *chp, int argc, char *argv[]) {
  size_t n, size;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: mem\r\n");
    return;
  }
  n = chHeapStatus(NULL, &size);
  chprintf(chp, "core free memory : %u bytes\r\n", chCoreStatus());
  chprintf(chp, "heap fragments   : %u\r\n", n);
  chprintf(chp, "heap free total  : %u bytes\r\n", size);
}

static void cmd_threads(BaseSequentialStream *chp, int argc, char *argv[]) {
  static const char *states[] = {THD_STATE_NAMES};
  Thread *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: threads\r\n");
    return;
  }
  chprintf(chp, "    addr    stack prio refs     state"
#if CH_DBG_THREADS_PROFILING
           "       time"
#endif
#if CH_USE_REGISTRY
           " name"
#endif
           "\r\n");
  tp = chRegFirstThread();
  do {
    chprintf(chp, "%.8lx %.8lx %4lu %4lu %9s"
#if CH_DBG_THREADS_PROFILING
             " %10lu"
#endif
#if CH_USE_REGISTRY
             " %s"
#endif
             "\r\n",
             (uint32_t)tp, (uint32_t)tp->p_ctx.r13,
             (uint32_t)tp->p_prio, (uint32_t)(tp->p_refs - 1),
             states[tp->p_state]
#if CH_DBG_THREADS_PROFILING
             , (uint32_t)tp->p_time
#endif
#if CH_USE_REGISTRY
             , chRegGetThreadName(tp)
#endif
             );
    tp = chRegNextThread(tp);
  } while (tp != NULL);
}

#if WITH_TEST

#define TEST_WA_SIZE    THD_WA_SIZE(256)

static void cmd_test(BaseSequentialStream *chp, int argc, char *argv[]) {
  Thread *tp;

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: test\r\n");
    return;
  }
  tp = chThdCreateFromHeap(NULL, TEST_WA_SIZE, chThdGetPriority(),
                           TestThread, chp);
  if (tp == NULL) {
    chprintf(chp, "out of memory\r\n");
    return;
  }
  chThdWait(tp);
}

#endif

static void cmd_write(BaseSequentialStream *chp, int argc, char *argv[]) {
  static uint8_t buf[] =
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef"
      "0123456789abcdef0123456789abcdef0123456789abcdef0123456789abcdef";

  (void)argv;
  if (argc > 0) {
    chprintf(chp, "Usage: write\r\n");
    return;
  }

  while (chnGetTimeout((BaseChannel *)chp, TIME_IMMEDIATE) == Q_TIMEOUT) {
    chSequentialStreamWrite(&SDU1, buf, sizeof(buf) - 1);
  }
  chprintf(chp, "\r\n\nstopped\r\n");
}

static inline uint8_t hex2int(char c){
  return c >= '0' && c <= '9' ? c - '0' : c >= 'a' && c <= 'f' ? c - 'a' + 10 : c >= 'A' && c <= 'F' ? c - 'A' + 10 : 16;
}

static inline char int2hex(uint8_t b){
  return b < 10 ? b + '0' : b < 16 ? b + 'a' - 10 : '\0';
}

static bool_t getHex(const char *str, uint8_t len, uint8_t *val){
  for(; len > 0 && *str != '\0'; len--){
    *val = hex2int(*str++) << 4;
    *val |= hex2int(*str++);
    if((*val & 0x0f) > 15 || (*val & 0xf0) > (15 << 4)){
      return FALSE;
    }
    val++;
  }
  if(len > 0){
    return FALSE;
  }
  return TRUE;
}

static uint8_t buf[255];

static void cmd_rf22_read(BaseSequentialStream *chp, int argc, char *argv[]) {
  uint8_t addr, len = 1, i;
  
  if (argc < 1 || argc > 2) {
    chprintf(chp, "Usage: rf22r <addr> [<size>] # read register\r\n");
    return;
  }
  
  if(!getHex(argv[0], 1, &addr) || addr > 127){
    chprintf(chp, "Invalid address: %s\r\n", argv[0]);
    return;
  }
  
  if(argc > 1){
    if((len = atoi(argv[1])) > 64){
      chprintf(chp, "Invalid size: %s\r\n", argv[1]);
      return;
    }
  }
  
  rf22RegisterRead(addr, len, buf);
  
  for(i = 0; i < len; i++){
    chprintf(chp, "%c%c", int2hex((buf[i] & 0xf0) >> 4), int2hex(buf[i] & 0x0f));
  }
  
  chprintf(chp, "\r\n");
}

static void cmd_rf22_write(BaseSequentialStream *chp, int argc, char *argv[]) {
  uint8_t addr, len;
  
  if (argc != 2) {
    chprintf(chp, "Usage: rf22w <addr> <data> # write register\r\n");
    return;
  }
  
  if(!getHex(argv[0], 1, &addr) || addr > 127){
    chprintf(chp, "Invalid address: %s\r\n", argv[0]);
    return;
  }
  
  if((len = strlen(argv[1]) / 2) > 64 || !getHex(argv[1], len, buf)){
    chprintf(chp, "Invalid value: %s\r\n", argv[1]);
    return;
  }
  
  rf22RegisterWrite(addr, len, buf);
  
  chprintf(chp, "OK\r\n");
}

static void cmd_rf22_restart(BaseSequentialStream *chp, int argc, char *argv[]) {
  (void)argv;
  
  if (argc != 0) {
    chprintf(chp, "Usage: rf22x # restart radio\r\n");
    return;
  }
  
  rf22LocalStop();
  rf22LocalStart();
}

static void cmd_rf22_send(BaseSequentialStream *chp, int argc, char *argv[]) {
  (void)argv;
  
  if (argc != 2 && argc != 3) {
    chprintf(chp, "Usage: rf22s <addr> <data> [repeat] # send\r\n");
    return;
  }
  
  uint8_t i, len, all;
  RF22Header addr;
  
  if(!getHex(argv[0], 4, &addr)){
    chprintf(chp, "Invalid address: %s\r\n", argv[0]);
    return;
  }
  
  for(i = 0; i < sizeof(buf) && argv[1][i << 1] != '\0'; i++){
    if(!getHex(&argv[1][i << 1], 1, &buf[i])){
      chprintf(chp, "Invalid data: \"%s\"\r\n", &argv[1][i << 1]);
      return;
    }
  }
  
  len = i;
  all = len;
  
  if(argc > 2){
    all *= atoi(argv[2]);
  }
  
  if(all >= sizeof(buf)){
    chprintf(chp, "Too much data (%u bytes)\r\n", all);
    return;
  }
  
  for(i = len; i < all; i += len){
    memcpy(buf+i, buf, len);
  }
  
  chprintf(chp, "send to: %x\r\n", addr);

  RF22Status status = rf22PacketSend(&RF22D1, all, buf, addr);

  chprintf(chp, "%s\r\n", status == RF22Success ? "success" : status == RF22Underflow ? "underflow" : status == RF22Overflow ? "overflow" : status == RF22Invalid ? "invalid" : status == RF22Timeout ? "timeout" : status == RF22Cancel ? "cancel" : "error");
}

static void cmd_rf22_listen(BaseSequentialStream *chp, int argc, char *argv[]) {
  (void)argv;
  
  if (argc > 1) {
    chprintf(chp, "Usage: rf22l [timeout] # listen\r\n");
    return;
  }
  
  systime_t timeout = TIME_INFINITE;
  
  if(argc > 0){
    timeout = MS2ST(atoi(argv[0]));
  }

  uint8_t i, len = sizeof(buf);
  RF22Status status = rf22PacketReceiveTimeout(&RF22D1, &len, buf, 0, timeout);
  
  if(status == RF22Success ||
     status == RF22Underflow ||
     status == RF22Overflow){
    for(i = 0; i < len; i++){
      chprintf(chp, "%c%c", int2hex((buf[i] & 0xf0) >> 4), int2hex(buf[i] & 0x0f));
    }
    chprintf(chp, "\r\n");
  }
  
  chprintf(chp, "%s\r\n", status == RF22Success ? "success" : status == RF22Underflow ? "underflow" : status == RF22Overflow ? "overflow" : status == RF22Invalid ? "invalid" : status == RF22Timeout ? "timeout" : status == RF22Cancel ? "cancel" : "error");
}

static void cmd_rf22_ping(BaseSequentialStream *chp, int argc, char *argv[]) {
  (void)argv;
  
  if(argc != 1 && argc != 2){
    chprintf(chp, "Usage: rf22p <addr> [len] # ping\r\n");
    return;
  }

  RF22Header addr;

  if(!getHex(argv[0], 4, &addr)){
    chprintf(chp, "Invalid address: %s\r\n", argv[0]);
    return;
  }
  
  uint8_t len = 4;
  
  if(argc > 1){
    len = atoi(argv[1]);
    if(len < 4){
      len = 4;
    }
  }
  
  chprintf(chp, "send ping to: %x\r\n", addr);
  
  halrtcnt_t lat;
  
  if(rf22LocalPing(addr, len, &lat)){
    chprintf(chp, "pong with latency: %u ms\r\n", RTT2MS(lat));
  }else{
    chprintf(chp, "ping timeout, latency: %u ms\r\n", RTT2MS(lat));
  }
}

static const ShellCommand commands[] = {
  {"mem", cmd_mem},
  {"threads", cmd_threads},
#if WITH_TEST
  {"test", cmd_test},
#endif
  {"write", cmd_write},
  {"rf22r", cmd_rf22_read},
  {"rf22w", cmd_rf22_write},
  {"rf22s", cmd_rf22_send},
  {"rf22l", cmd_rf22_listen},
  {"rf22x", cmd_rf22_restart},
  {"rf22p", cmd_rf22_ping},
  {NULL, NULL}
};

static const ShellConfig shell_cfg1 = {
  (BaseSequentialStream *)&SDU1,
  commands
};

/*===========================================================================*/
/* Generic code.                                                             */
/*===========================================================================*/

/*
 * Red LED blinker thread, times are in milliseconds.
 */
static WORKING_AREA(waThread1, 128);
static msg_t Thread1(void *arg) {

  (void)arg;
  chRegSetThreadName("blinker");
  while (TRUE) {
    systime_t time = usbLocalState() ? 250 : 500;
    boardLedOn(STATUS);
    chThdSleepMilliseconds(time);
    boardLedOff(STATUS);
    chThdSleepMilliseconds(time);
  }

  return 0;
}

/*
 * Application entry point.
 */
int main(void) {
  Thread *shelltp = NULL;

  /*
   * System initializations.
   * - HAL initialization, this also initializes the configured device drivers
   *   and performs the board-specific initializations.
   * - Kernel initialization, the main() function becomes a thread and the
   *   RTOS is active.
   */
  halInit();
  chSysInit();

  /*
   * USB interface initialization.
   */
  usbLocalInit();
  usbLocalStart();
  
  /*
   * EXT interrupts initialization.
   */
  extStart(&EXTD1, &extcfg);
  
  /*
   * RF22 device initialization.
   */
  rf22LocalInit();
  rf22LocalStart();
  
  /*
   * Shell manager initialization.
   */
  shellInit();

  /*
   * Creates the blinker thread.
   */
  chThdCreateStatic(waThread1, sizeof(waThread1), NORMALPRIO, Thread1, NULL);

  /*
   * Normal main() thread activity, in this demo it does nothing except
   * sleeping in a loop and check the button state.
   */
  while (TRUE) {
    if (!shelltp && (SDU1.config->usbp->state == USB_ACTIVE))
      shelltp = shellCreate(&shell_cfg1, SHELL_WA_SIZE, NORMALPRIO);
    else if (chThdTerminated(shelltp)) {
      chThdRelease(shelltp);    /* Recovers memory of the previous shell.   */
      shelltp = NULL;           /* Triggers spawning of a new shell.        */
    }
    chThdSleepMilliseconds(1000);
  }
}
