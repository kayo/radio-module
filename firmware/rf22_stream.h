/**
 * @file
 * @brief The sequential stream interface for RF22 driver.
 *
 * @defgroup rf22stm RF22 Sequential Stream
 * @{
 */

/**
 * @brief RF22 Connection Options.
 */
typedef struct {
  /**
   * @brief The radio driver.
   */
  RF22Driver *radio;
  
  /**
   * @brief The receive listening timeout.
   */
  systime_t timeout;
#if 0  
  /**
   * @brief The connection ping period.
   */
  systime_t period;
#endif
} RF22ConnectionOptions;

/**
 * @brief RF22 Sequential Stream object.
 */
typedef struct {
  /**
   * @brief The base sequential stream virtual method table.
   */
  const struct BaseSequentialStreamVMT *vmt;
  _base_sequential_stream_data

  /**
   * @brief The connection options.
   */
  const RF22ConnectionOptions *opt;
  
  /**
   * @brief The radio header to send.
   */
  RF22Header header;
} RF22SequentialStream;

/**
 * @brief Open outgoing sequential stream connection using rf22 driver.
 *
 * @param stm The pointer to used sequential stream.
 * @param header The packet header to use (ex. source/destination mac address).
 * @param opt The connection options.
 */
void rf22SequentialStreamOpen(RF22SequentialStream *stm, RF22Header header, const RF22ConnectionOptions *opt);

/**
 * @}
 */
