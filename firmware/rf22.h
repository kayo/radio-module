/**
 * @file
 * @brief RF22 Driver
 *
 * @defgroup rf22 RF22 ChibiOS Driver
 * @{
 *
 * Deeply engineered from scratch RF22 (Si4432) Driver for ChibiOS.
 * This driver supports almost all configuration options which supports target hardware.
 */

#define RF22_KHZ(val) ((val)*1000)
#define RF22_MHZ(val) ((val)*1000000)

/**
 * @brief Enable using mutual exclusion.
 */
#ifndef RF22_USE_MUTUAL_EXCLUSION
#define RF22_USE_MUTUAL_EXCLUSION TRUE
#endif

#ifndef RF22_IF_FILTER_BW
#define RF22_IF_FILTER_BW 2
#endif

/**
 * @brief Enable using embedded bandwidth tables.
 */
#ifndef RF22_USE_BW_TABLES
#define RF22_USE_BW_TABLES TRUE
#endif

/**
 * @brief Enable ack transport functions.
 */
#ifndef RF22_WITH_ACK
#define RF22_WITH_ACK FALSE
#endif

/**
 * @brief Enable using predefined modem configurations instead of runtime.
 */
#ifndef RF22_USE_PREDEF_MODEM_CONFIG
#define RF22_USE_PREDEF_MODEM_CONFIG FALSE
#endif

enum {
  RF22AfcPullInRangeDefault = 1000000
};

#if RF22_USE_PREDEF_MODEM_CONFIG

/**
 * @brief Modem config
 */
typedef PACK_STRUCT_BEGIN struct {
  /**
   * @brief Register IF_FILTER_BANDWIDTH (1c)
   */
  uint8_t if_filter_bandwidth;
  /**
   * @brief Registers from CLOCK_RECOVERY_GEARSHIFT_OVERRIDE to CLOCK_RECOVERY_TIMING_LOOP_GAIN0
   * 
   * CLOCK_RECOVERY_GEARSHIFT_OVERRIDE (1f)
   * CLOCK_RECOVERY_OVERSAMPLING_RATE (20)
   * CLOCK_RECOVERY_OFFSET2 (21)
   * CLOCK_RECOVERY_OFFSET1 (22)
   * CLOCK_RECOVERY_OFFSET0 (23)
   * CLOCK_RECOVERY_TIMING_LOOP_GAIN1 (24)
   * CLOCK_RECOVERY_TIMING_LOOP_GAIN0 (25)
   */
  uint8_t clock_recovery[7];
  /**
   * @brief Registers from OOK_COUNTER_VALUE_1 to SLICER_PEAK_HOLD
   *
   * OOK_COUNTER_VALUE_1 (2c)
   * OOK_COUNTER_VALUE_2 (2d)
   * SLICER_PEAK_HOLD (2e)
   */
  uint8_t ook_counter_value[3];
  /**
   * @brief Register CHARGE_PUMP_CURRENT_TRIMMING (58)
   */
  uint8_t change_pump_current_trimming;
  /**
   * @brief Register AGC_OVERRIDE1 (69)
   */
  uint8_t agc_override1;
  /**
   * @brief Registers TX_DATA_RATE1 and TX_DATA_RATE0
   *
   * TX_DATA_RATE1 (6e)
   * TX_DATA_RATE0 (6f)
   */
  uint8_t tx_data_rate[2];
  /**
   * @brief Registers from MODULATION_CONTROL1 to FREQUENCY_DEVIATION
   *
   * MODULATION_CONTROL1 (70)
   * MODULATION_CONTROL2 (71)
   * FREQUENCY_DEVIATION (72)
   */
  uint8_t modulation_control[3];
} PACK_STRUCT_STRUCT RF22ModemConfig PACK_STRUCT_END;

/**
 * @brief Unmodulated carrier
 *
 * Predefined modem configurations:
 *
 * Rb - bitrate in Kbps
 * Fd - frequency deviation in KHz
 */
#define RF22NoModulation { 0x2b, {0x03, 0xf4, 0x20, 0x41, 0x89, 0x00, 0x36}, {0x40, 0x0a, 0x1d}, 0x80, 0x60, {0x10, 0x62}, {0x2c, 0x00, 0x08} }

/**
 * @brief FSK, No Manchester, Rb=2Kbps, Fd=5KHz, PN9 random modulation
 */
#define RF22Fsk2Kbps5KHzPn9 { 0x2b, {0x03, 0xf4, 0x20, 0x41, 0x89, 0x00, 0x36}, {0x40, 0x0a, 0x1d}, 0x80, 0x60, {0x10, 0x62}, {0x2c, 0x33, 0x08} }
/**
 * @brief FSK, No Manchester, Rb=2Kbps, Fd=5KHz
 */
#define RF22Fsk2Kbps5KHz { 0x2b, {0x03, 0xf4, 0x20, 0x41, 0x89, 0x00, 0x36}, {0x40, 0x0a, 0x1d}, 0x80, 0x60, {0x10, 0x62}, {0x2c, 0x22, 0x08} }
/**
 * @brief FSK, No Manchester, Rb=2.4Kbps, Fd=36KHz
 */
#define RF22Fsk2p4Kbps36KHz { 0x1b, {0x03, 0x41, 0x60, 0x27, 0x52, 0x00, 0x07}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x13, 0xa9}, {0x2c, 0x22, 0x3a} }
/**
 * @brief FSK, No Manchester, Rb=4.8Kbps, Fd=45KHz
 */
#define RF22Fsk4p8Kbps45KHz { 0x1d, {0x03, 0xa1, 0x20, 0x4e, 0xa5, 0x00, 0x13}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x27, 0x52}, {0x2c, 0x22, 0x48} }
/**
 * @brief FSK, No Manchester, Rb=9.6Kbps, Fd=45KHz
 */
#define RF22Fsk9p6Kbps45KHz { 0x1e, {0x03, 0xd0, 0x00, 0x9d, 0x49, 0x00, 0x45}, {0x40, 0x0a, 0x20}, 0x80, 0x60, {0x4e, 0xa5}, {0x2c, 0x22, 0x48} }
/**
 * @brief FSK, No Manchester, Rb=19.2Kbps, Fd=9.6KHz
 */
#define RF22Fsk19p2Kbps9p6KHz { 0x2b, {0x03, 0x34, 0x02, 0x75, 0x25, 0x07, 0xff}, {0x40, 0x0a, 0x1b}, 0x80, 0x60, {0x9d, 0x49}, {0x2c, 0x22, 0x0f} }
/**
 * @brief FSK, No Manchester, Rb=38.4Kbps, Fd=19.6KHz
 */
#define RF22Fsk38p4Kbps19p6KHz { 0x02, {0x03, 0x68, 0x01, 0x3a, 0x93, 0x04, 0xd5}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x09, 0xd5}, {0x0c, 0x22, 0x1f} }
/**
 * @brief FSK, No Manchester, Rb=57.6Kbps, Fd=28.8KHz
 */
#define RF22Fsk57p6Kbps28p8KHz { 0x06, {0x03, 0x45, 0x01, 0xd7, 0xdc, 0x07, 0x6e}, {0x40, 0x0a, 0x2d}, 0x80, 0x60, {0x0e, 0xbf}, {0x0c, 0x22, 0x2e} }
/**
 * @brief GFSK, No Manchester, Rb=100Kbps, Fd=50KHz
 */
#define RF22Fsk100Kbps50KHz { 0x9a, {0x03, 0x3c, 0x02, 0x22, 0x22, 0x07, 0xff}, {0x28, 0x0c, 0x28}, 0xc0, 0x60, {0x19, 0x9a}, {0x0c, 0x22, 0x50} }
/**
 * @brief FSK, No Manchester, Rb=125Kbps, Fd=125KHz
 */
#define RF22Fsk125Kbps125KHz { 0x8a, {0x03, 0x60, 0x01, 0x55, 0x55, 0x02, 0xad}, {0x40, 0x0a, 0x50}, 0x80, 0x60, {0x20, 0x00}, {0x0c, 0x22, 0xc8} }
/**
 * @brief FSK, No Manchester, Rb=512bps, Fd=2.5KHz (POCSAG compat)
 */
#define RF22Fsk512bps2p5KHz { 0x2b, {0x03, 0xa1, 0xe0, 0x10, 0xc7, 0x00, 0x09}, {0x40, 0x0a, 0x1d},  0x80, 0x60, {0x04, 0x32}, {0x2c, 0x22, 0x04} }
/**
 * @brief FSK, No Manchester, Rb=512bps, Fd=4.5KHz (POCSAG compat)
 */
#define RF22Fsk512bps4p5KHz { 0x27, {0x03, 0xa1, 0xe0, 0x10, 0xc7, 0x00, 0x06}, {0x40, 0x0a, 0x1d},  0x80, 0x60, {0x04, 0x32}, {0x2c, 0x22, 0x07} }

/**
 * @brief GFSK, No Manchester, Rb=2Kbps, Fd=5KHz
 */
#define RF22Gfsk2Kbps5KHz { 0x2b, {0x03, 0xf4, 0x20, 0x41, 0x89, 0x00, 0x36}, {0x40, 0x0a, 0x1d}, 0x80, 0x60, {0x10, 0x62}, {0x2c, 0x23, 0x08} }
/**
 * @brief GFSK, No Manchester, Rb=2.4Kbps, Fd=36KHz
 */
#define RF22Gfsk2p4Kbps36KHz { 0x1b, {0x03, 0x41, 0x60, 0x27, 0x52, 0x00, 0x07}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x13, 0xa9}, {0x2c, 0x23, 0x3a} }
/**
 * @brief GFSK, No Manchester, Rb=4.8Kbps, Fd=45KHz
 */
#define RF22Gfsk4p8Kbps45KHz { 0x1d, {0x03, 0xa1, 0x20, 0x4e, 0xa5, 0x00, 0x13}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x27, 0x52}, {0x2c, 0x23, 0x48} }
/**
 * @brief GFSK, No Manchester, Rb=9.6Kbps, Fd=45KHz
 */
#define RF22Gfsk9p6Kbps45KHz { 0x1e, {0x03, 0xd0, 0x00, 0x9d, 0x49, 0x00, 0x45}, {0x40, 0x0a, 0x20}, 0x80, 0x60, {0x4e, 0xa5}, {0x2c, 0x23, 0x48} }
/**
 * @brief GFSK, No Manchester, Rb=19.2Kbps, Fd=9.6KHz
 */
#define RF22Gfsk19p2Kbps9p6KHz { 0x2b, {0x03, 0x34, 0x02, 0x75, 0x25, 0x07, 0xff}, {0x40, 0x0a, 0x1b}, 0x80, 0x60, {0x9d, 0x49}, {0x2c, 0x23, 0x0f} }
/**
 * @brief GFSK, No Manchester, Rb=38.4Kbps, Fd=19.6KHz
 */
#define RF22Gfsk38p4Kbps19p6KHz { 0x02, {0x03, 0x68, 0x01, 0x3a, 0x93, 0x04, 0xd5}, {0x40, 0x0a, 0x1e}, 0x80, 0x60, {0x09, 0xd5}, {0x0c, 0x23, 0x1f} }
/**
 * @brief GFSK, No Manchester, Rb=57.6Kbps, Fd=28.8KHz
 */
#define RF22Gfsk57p6Kbps28p8KHz { 0x06, {0x03, 0x45, 0x01, 0xd7, 0xdc, 0x07, 0x6e}, {0x40, 0x0a, 0x2d}, 0x80, 0x60, {0x0e, 0xbf}, {0x0c, 0x23, 0x2e} }
/**
 * @brief GFSK, No Manchester, Rb=100Kbps, Fd=50KHz
 */
#define RF22Gfsk100Kbps50KHz { 0x9a, {0x03, 0x3c, 0x02, 0x22, 0x22, 0x07, 0xff}, {0x28, 0x0c, 0x28}, 0xc0, 0x60, {0x19, 0x9a}, {0x0c, 0x23, 0x50} }
/**
 * @brief GFSK, No Manchester, Rb=125Kbps, Fd=125KHz
 */
#define RF22Gfsk125Kbps125KHz { 0x8a, {0x03, 0x60, 0x01, 0x55, 0x55, 0x02, 0xad}, {0x40, 0x0a, 0x50}, 0x80, 0x60, {0x20, 0x00}, {0x0c, 0x23, 0xc8} }

/**
 * @brief OOK, No Manchester, Rb=1.2Kbps, Rx Bandwidth=75KHz
 */
#define RF22Ook1p2Kbps75KHz { 0x51, {0x03, 0x68, 0x00, 0x3a, 0x93, 0x01, 0x3d}, {0x2c, 0x11, 0x28}, 0x80, 0x60, {0x09, 0xd5}, {0x2c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=2.4Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook2p4Kbps335KHz { 0xc8, {0x03, 0x39, 0x20, 0x68, 0xdc, 0x00, 0x6b}, {0x2a, 0x08, 0x2a}, 0x80, 0x60, {0x13, 0xa9}, {0x2c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=4.8Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook4p8Kbps335KHz { 0xc8, {0x03, 0x9c, 0x00, 0xd1, 0xb7, 0x00, 0xd4}, {0x29, 0x04, 0x29}, 0x80, 0x60, {0x27, 0x52}, {0x2c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=9.6Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook9p6Kbps335KHz { 0xb8, {0x03, 0x9c, 0x00, 0xd1, 0xb7, 0x00, 0xd4}, {0x28, 0x82, 0x29}, 0x80, 0x60, {0x4e, 0xa5}, {0x2c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=19.2Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook19p2Kbps335KHz { 0xa8, {0x03, 0x9c, 0x00, 0xd1, 0xb7, 0x00, 0xd4}, {0x28, 0x41, 0x29}, 0x80, 0x60, {0x9d, 0x49}, {0x2c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=38.4Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook38p4Kbps335KHz { 0x98, {0x03, 0x9c, 0x00, 0xd1, 0xb7, 0x00, 0xd4}, {0x28, 0x20, 0x29}, 0x80, 0x60, {0x09, 0xd5}, {0x0c, 0x21, 0x08} }
/**
 * @brief OOK, No Manchester, Rb=40Kbps, Rx Bandwidth=335KHz
 */
#define RF22Ook40Kbps335KHz { 0x98, {0x03, 0x96, 0x00, 0xda, 0x74, 0x00, 0xdc}, {0x28, 0x1f, 0x29}, 0x80, 0x60, {0x0a, 0x3d}, {0x0c, 0x21, 0x08} }

#else/*RF22_USE_PREDEF_MODEM_CONFIG*/

/**
 * @brief Modulation type
 */
typedef enum {
  /**
   * @brief Unmodulated carrier
   */
  RF22ModulationNone = 0,
  /**
   * @brief On-Off Keying
   */
  RF22ModulationOOK,
  /**
   * @brief Frequency Shift Keying
   */
  RF22ModulationFSK,
  /**
   * @brief Gaussian Frequency Shift Keying
   */
  RF22ModulationGFSK,
} RF22ModulationType;

/**
 * @brief Modulation data source
 */
typedef enum {
  /**
   * @brief Direct Mode using TX/RX Data via GPIO pin
   * (GPIO configuration required)
   */
  RF22DataSourceDirectGPIO = 0,
  /**
   * @brief Direct Mode using TX/RX Data via SDI pin
   * (only when nSEL is high)
   */
  RF22DataSourceDirectSDI,
  /**
   * @brief FIFO Mode
   */
  RF22DataSourceFIFO,
  /**
   * @brief PN9
   * (internally generated)
   */
  RF22DataSourcePN9,
} RF22DataSource;

/**
 * @brief Modulation data clock
 *
 * Only when direct data source is used.
 */
typedef enum {
  /**
   * @brief No TX Data CLK is available.
   * 
   * Asynchronous mode – сan only work with modulations FSK or OOK.
   */
  RF22DataClockNone = 0,
  /**
   * @brief TX Data CLK is available via the GPIO.
   *
   * One of the GPIOs should be programmed as well.
   */
  RF22DataClockGPIO,
  /**
   * @brief TX Data CLK is available via the SDO pin.
   */
  RF22DataClockSDO,
  /**
   * @brief TX Data CLK is available via the nIRQ pin.
   */
  RF22DataClockNIRQ,
} RF22DataClock;

/**
 * @brief Manchester coding options.
 */
typedef struct {
  /**
   * @brief Enable manchester encoding.
   *
   * Manchester encoding replaces all bits in data sequence onto bits pairs with zero to one or one to zero transitions (ex.: 0 to 1->0 and 1 to 0->1).
   */
  bool_t enable;
  /**
   * @brief Enable manchester data inversion.
   *
   * Inverts manchester bits pair transitions.
   */
  bool_t inversion;
  /**
   * @brief Manchester preamble polarity.
   *
   * Will transmit a series of 1 if set, or series of 0 if reset.
   */
  bool_t preamblePolarity;
} RF22ManchesterConfig;

/**
 * @brief The modulation options.
 */
typedef struct {
  /**
   * @brief The modulation type.
   */
  RF22ModulationType type;
  /**
   * @brief The data source.
   */
  RF22DataSource dataSource;
  /**
   * @brief The transmit data clock.
   */
  RF22DataClock dataClock;
  /**
   * @brief The transmit data rate.
   *
   * A valid data rate in range from 125 to 256e3 bps (from 125 bps to 256 kbps).
   */
  uint32_t dataRate;
  /**
   * @brief Invert TX and RX Data.
   */
  bool_t dataInversion;
  
  /**
   * @brief Enable data whitening.
   *
   * Whitening means XORing data with random value to avoid long sequences of of zeros or ones.
   */
  bool_t whitening;
  
  /**
   * @brief Manchester coding options.
   */
  RF22ManchesterConfig manchester;
  
  union {
    struct {
      /**
       * @brief Use automatic frequency compensation.
       */
      bool_t afcEnable;
      
      /**
       * @brief Automatic frequency control pull in range.
       * 
       * A valid AFC pull range is up to 159375 Hz for hi-band (>= 480 MHz) and up to 318750 Hz for lo-band (< 480 MHz).
       * If RF22_IF_FILTER_BW is 2 you can use RF22AfcPullInRangeDefault as default value.
       */
      uint32_t afcPullInRange;
      
      /**
       * @brief Frequency deviation.
       *
       * Value in range from 625 to 320000 Hz (50000 Hz by default)
       */
      uint32_t frequencyDeviation;
    } fsk;
    
    struct {
      /**
       * @brief 
       */
      uint16_t bandWidth; /* 75..620 (200) [KHz] */
    } ook;
  };
} RF22ModemConfig;

#endif/*RF22_USE_PREDEF_MODEM_CONFIG*/

/**
 * @brief Data bits order
 */
typedef enum {
  /**
   * @brief Most significant bit first.
   */
  RF22BitsOrderMSB,
  /**
   * @brief Least significant bit first.
   */
  RF22BitsOrderLSB,
} RF22BitsOrder;

/**
 * @brief Cyclic redundancy check option.
 */
typedef enum {
  RF22CrcNone = 0,
  RF22CrcCCIT,
  RF22Crc16IBM,
  RF22Crc16IEC,
  RF22CrcBIACHEVA
} RF22CrcOption;

/**
 * @brief Cyclic redundancy check region.
 */
typedef enum {
  RF22CrcPacket,
  RF22CrcDataOnly,
} RF22CrcRegion;

/**
 * @brief Cyclic redundancy check config.
 */
typedef struct {
  /**
   * @brief CRC option to use.
   */
  RF22CrcOption option;
  /**
   * @brief Crc calculation region.
   */
  RF22CrcRegion region;
} RF22CrcConfig;

/**
 * @brief Preamble detection config.
 */
typedef struct {
  /**
   * @brief Preamble length (bits).
   *
   * Actually reduces to number of nibbles (4 bits) of preamble in the packet.
   *
   * For example 000001000 corresponds to a preamble length of 32 bits (8 x 4bits) or 4 bytes. The maximum preamble length is 111111111 which corresponds to a 255 bytes Preamble. Writing 0 will have the same result as if writing 1, which corresponds to one single nibble of preamble.
   */
  uint16_t length;
  /**
   * @brief Preamble detection threshold (bits).
   *
   * Actually reduces to number of nibbles (4 bits) of preamble pattern (i.e., 01010...) that must be received correctly, before a PREAMBLE_VALID signal is issued. This threshold helps guard against false preamble detection upon noise.
   */
  uint16_t threshold;
  /**
   * @brief Skip 2nd phase of preamble detection.
   */
  bool_t skip2Phase;
} RF22PreambleConfig;

/**
 * @brief Eight bits length type.
 */
typedef uint8_t RF22Length;

/**
 * @brief Four byte header type.
 */
typedef uint32_t RF22Header;

/**
 * @brief Data buffer type.
 */
typedef uint8_t RF22Buffer;

/**
 * @brief Synchronization word config.
 */
typedef struct {
  /**
   * @brief Length of the sync word.
   *
   * Valid range of word is 1-4 bytes.
   */
  RF22Length length;
  /**
   * @brief Contents of the sync word.
   */
  RF22Header value;
  /**
   * @brief Length of the sync word.
   */
  bool_t skipSearchTimeout;
} RF22SyncWordConfig;

/**
 * @brief Packet header config.
 */
typedef struct {
  /**
   * @brief Length of the header.
   *
   * Valid range of word is 0-4 bytes.
   */
  RF22Length length;
  
  /**
   * @brief Packet header to check.
   */
  struct {
    /**
     * @brief Contents of the header to check.
     */
    RF22Header value;
    /**
     * @brief Bit mask of the header to check.
     */
    RF22Header mask;
    /**
     * @brief Default broadcast state.
     */
    bool_t broadcast;
  } check;
  
  /**
   * @brief Packet header to send.
   */
  struct {
    /**
     * @brief Contents of the header to send.
     */
    RF22Header value;
  } send;
} RF22HeaderConfig;

enum {
  /**
   * @brief Variable Data Length constant.
   */
  RF22DataLengthVariable = 0
};

/**
 * @brief Hardware packet handling. 
 *
 * Common packet structure
 *
 * Preamble (1-255 bytes) | Sync Word (1-4 bytes) | Header (0-4 bytes) | Data Length (0 or 1 byte) | Data | CRC (0 or 2 bytes)
 *
 */
typedef struct {
  /**
   * @brief Enable TX hardware packet handling. 
   */
  bool_t handleTx;
  /**
   * @brief Enable RX hardware packet handling. 
   */
  bool_t handleRx;
  /**
   * @brief Packet bits ordering. 
   */
  RF22BitsOrder bitsOrder;
  
  /**
   * @brief Cyclic redundancy check.
   */
  RF22CrcConfig crc;
  
  /**
   * @brief Preamble detection options.
   */
  RF22PreambleConfig preamble;
  
  /**
   * @brief Synchronization word options.
   */
  RF22SyncWordConfig syncWord;

  /**
   * @brief Packet header options.
   */
  RF22HeaderConfig header;
  
  /**
   * @brief Packet data length.
   *
   * Fixed length in range from _ to _.
   * 0 - means use variable packet length.
   */
  uint8_t dataLength;
} RF22PacketConfig;

/**
 * @brief Automatic FIFO control.
 */
typedef struct {
  /**
   * @brief The transmit fifo almost full threshold.
   *
   * A valid threshold value in range from 0 to 64 bytes.
   */
  uint8_t txAlmostFull;
  /**
   * @brief The transmit fifo almost empty threshold.
   *
   * A valid threshold value in range from 0 to 64 bytes.
   */
  uint8_t txAlmostEmpty;
  /**
   * @brief The receive fifo almost full threshold.
   *
   * A valid threshold value in range from 0 to 64 bytes.
   */
  uint8_t rxAlmostFull;
} RF22FifoConfig;

/**
 * @brief Rx/Tx LED config.
 */
typedef enum {
  /**
   * @brief Don't use this led.
   */
  RF22LedOff,
  /**
   * @brief Active is 0.
   */
  RF22LedOnLo,
  /**
   * @brief Active is 1.
   */
  RF22LedOnHi,
} RF22LedMode;

/**
 * @brief Rx/Tx LEDs.
 */
typedef struct {
  /**
   * @brief The led port.
   */
  ioportid_t port;
  /**
   * @brief The led pad number.
   */
  uint16_t pad;
  /**
   * @brief The led mode.
   */
  RF22LedMode mode;
} RF22LedConfig;

/**
 * @brief GPIO Function.
 */
typedef enum {
  RF22Gpio0PowerOnReset = 0,
  RF22Gpio1InvPowerOnReset = 0,
  RF22Gpio2McuClock = 0,
  
  RF22GpioWakeupTimer,
  RF22GpioLowBatteryDetect,
  RF22GpioDirectDigitalInput,
  
  RF22GpioAnalogInput = 7,
  RF22GpioDirectDigitalOutput = 10,
  RF22GpioReferenceVoltage = 14,
  RF22GpioRxTxDataClockOutput,
  RF22GpioDirectTxDataInput = 16,
  RF22GpioTxStateOutput = 18,
  RF22GpioRxStateOutput = 21,
  
  RF22GpioVDD = 29,
  RF22GpioGND = 30,
} RF22GpioOption;

/**
 * @brief GPIO Config.
 */
typedef struct {
  uint8_t driving;
  bool_t pullup;
  RF22GpioOption option;
} RF22GpioConfig;

/**
 * @brief The driver configuration.
 */
typedef struct {
  /**
   * @brief Used SPI driver.
   */
  SPIDriver *spip;
  /**
   * @brief Used SPI driver config.
   */
  const SPIConfig *spicfg;
  
  /**
   * @brief The shutdown (SDN) line config.
   */
  struct {
    /**
     * @brief The shutdown (SDN) line port.
     */
    ioportid_t port;
    /**
     * @brief The shutdown (SDN) line pad number.
     */
    uint16_t pad;
  } sdn;
  
  /**
   * @brief The indication LEDs config.
   */
  struct {
    RF22LedConfig tx;
    RF22LedConfig rx;
  } led;
  
  /**
   * @brief The carrier frequency.
   * 
   * A valid frequency in range from 240e6 to 960e6 Hz inclusive (240-960 MHz).
   */
  uint32_t carrierFrequency;
  
  /**
   * @brief The frequency hopping step.
   * 
   * 1 means 10KHz step, 2 - 20KHz, etc.
   */
  uint8_t hoppingStep;
  
  /**
   * @brief Default frequency hopping channel.
   */
  uint8_t hoppingChannel;
  
  /**
   * @brief Default transmiter power.
   */
  uint8_t txPower;
  
  /**
   * @brief The modulation/demodulation options.
   */
  RF22ModemConfig modem;
  
  /**
   * @brief Hardware packet handling. 
   */
  RF22PacketConfig packet;
  
  /**
   * @brief The FIFO options.
   */
  RF22FifoConfig fifoControl;

  /**
   * @brief GPIO config option.
   */
  RF22GpioConfig gpioConfig[3];

  /**
   * @brief Retransmissions count threshold.
   */
  uint8_t maxRetry;
  
} RF22Config;

/**
 * @brief The driver state.
 */
typedef enum {
  RF22_UNINIT = 0,
  RF22_STOP,
  RF22_READY,
  RF22_ERROR,
  RF22_TRANSMIT,
  RF22_LISTEN,
  RF22_RECEIVE,
} RF22State;

/**
 * @brief Data send status.
 */
typedef enum {
  /**
   * @brief Data sent or received successfully.
   */
  RF22Success,
  /**
   * @brief The error occurred when sending or receiving data.
   */
  RF22Error,
  /**
   * @brief Operation cancelled.
   */
  RF22Cancel,
  /**
   * @brief Timeout reached.
   */
  RF22Timeout,
  /**
   * @brief Invalid data received (CRC error).
   */
  RF22Invalid,
  /**
   * @brief Too little data in buffer (for send) or too little data in packet (for receive).
   */
  RF22Underflow,
  /**
   * @brief Too much data in buffer (for send) or too much data in packet (for receive).
   */
  RF22Overflow,
  /**
   * @brief Retransmissions limit exceeded.
   */
  RF22MaxRetry,
} RF22Status;

/**
 * @brief The driver structure.
 */
typedef struct {
  /**
   * @brief The driver configuration.
   */
  const RF22Config *config;
  
  /**
   * @brief Actual hopping channel.
   */
  uint8_t hoppingChannel;
  
  /**
   * @brief Actual tx power.
   */
  uint8_t txPower;
  
  /**
   * @brief Broadcast receive enable.
   */
  bool_t broadcast;
  
  /**
   * @brief Received signal strength indicator.
   */
  int8_t rssi;

  /**
   * @brief Driver state.
   */
  RF22State state;

  /**
   * @brief Event-waiting binary semaphore.
   */
  Semaphore eventSemaphore;

  /**
   * @brief Current header values
   */
  struct {
    RF22Header check;
    RF22Header send;
  } header;
  
#if RF22_USE_MUTUAL_EXCLUSION || defined(__DOXYGEN__)
  Thread *thread;
# if CH_USE_MUTEXES || defined(__DOXYGEN__)
  /**
   * @brief Exclusive access mutex.
   */
  Mutex mutex;
# elif CH_USE_SEMAPHORES || defined(__DOXYGEN__)
  /**
   * @brief Exclusive access semaphore.
   */
  Semaphore semaphore;
# endif
#endif
} RF22Driver;

/**
 * @brief Initialize driver structure.
 *
 * @param radio The driver pointer.
 */
void rf22ObjectInit(RF22Driver *radio);

/**
 * @brief Start driver, configure hardware and turn it to idle mode.
 *
 * @param radio The driver pointer.
 * @param config The configuration to use.
 *
 * The driver object must been initialized before starting it.
 */
void rf22Start(RF22Driver *radio, const RF22Config *config);

/**
 * @brief Shutdown hardware and stop driver.
 *
 * @param radio The driver pointer.
 *
 * The driver must been started before stopping it.
 */
void rf22Stop(RF22Driver *radio);

/**
 * @brief Change radio channel (frequency hopping).
 *
 * @param radio The driver pointer.
 * @param channel The target frequency channel.
 */
void rf22SetChannel(RF22Driver *radio, uint8_t channel);

/**
 * @brief Change power of transmitter.
 *
 * @param radio The driver pointer.
 * @param power The target transmitter power.
 */
void rf22SetPower(RF22Driver *radio, uint8_t power);

/**
 * @brief Switch broadcast mode.
 *
 * @param radio The driver pointer.
 * @param state The new broadcast state.
 */
void rf22SetBroadcast(RF22Driver *radio, bool_t state);

/**
 * @brief Send piece of data as packet.
 *
 * @param radio The driver pointer.
 * @param len The length of data to send.
 * @param buf The pointer to data buffer to send.
 * @param header The target header (ex. receiver mac address).
 */
RF22Status rf22PacketSend(RF22Driver *radio, RF22Length len, const RF22Buffer *buf, RF22Header header);

/**
 * @brief Receive packet with piece of data with timeout.
 *
 * @param radio The driver pointer.
 * @param len The maxumum length of data to receive.
 * @param buf The pointer to data buffer to write receiving data.
 * @param header The target header (ex. sender mac address).
 * @param timeout The listening timeout.
 */
RF22Status rf22PacketReceiveTimeout(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, RF22Header header, systime_t timeout);

/**
 * @brief Receive packet with piece of data.
 *
 * @param radio The driver pointer.
 * @param len The maxumum length of data to receive.
 * @param buf The pointer to data buffer to write receiving data.
 * @param header The target header (ex. sender mac address).
 */
static inline RF22Status rf22PacketReceive(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, RF22Header header){
  return rf22PacketReceiveTimeout(radio, len, buf, header, TIME_INFINITE);
}

/**
 * @brief Send and receive packet with piece of data with timeout.
 */
RF22Status rf22PacketSendReceiveTimeout(RF22Driver *radio, RF22Length send_len, const RF22Buffer *send_buf, RF22Header send_header, RF22Length *recv_len, RF22Buffer *recv_buf, RF22Header recv_header, systime_t recv_timeout);

/**
 * @brief Send and receive packet with piece of data.
 */
static inline RF22Status rf22PacketSendReceive(RF22Driver *radio, RF22Length send_len, const RF22Buffer *send_buf, RF22Header send_header, RF22Length *recv_len, RF22Buffer *recv_buf, RF22Header recv_header){
  return rf22PacketSendReceiveTimeout(radio, send_len, send_buf, send_header, recv_len, recv_buf, recv_header, TIME_INFINITE);
}

#if RF22_WITH_ACK
/**
 * @brief Send piece of data as packet with ack and timeout.
 */
RF22Status rf22PacketSendWithAckTimeout(RF22Driver *radio, RF22Length len, const RF22Buffer *buf, RF22Header header, systime_t timeout);

/**
 * @brief Send piece of data as packet with ack.
 */
static inline RF22Status rf22PacketSendWithAck(RF22Driver *radio, RF22Length len, const RF22Buffer *buf, RF22Header header){
  return rf22PacketSendWithAckTimeout(radio, len, buf, header, TIME_INFINITE);
}

/**
 * @brief Receive packet with piece of data with ack and timeout.
 */
RF22Status rf22PacketReceiveWithAckTimeout(RF22Driver *radio, RF22Length *len, RF22Buffer *buf, systime_t timeout);

/**
 * @brief Receive packet with piece of data with ack.
 */
static inline RF22Status rf22PacketReceiveWithAck(RF22Driver *radio, RF22Length *len, RF22Buffer *buf){
  return rf22PacketReceiveWithAckTimeout(radio, len, buf, TIME_INFINITE);
}
#endif/*RF22_WITH_ACK*/

/**
 * @brief External interrupt event handler.
 */
void rf22SendEventFromIsr(RF22Driver *radio);

/**
 * Low-level calls (use it for debug purpose only)
 */

/**
 * @brief Put byte(s) to hardware register(s).
 *
 * @param radio The driver pointer.
 * @param reg The hardware register address.
 * @param len The source data length in bytes.
 * @param buf The pointer to data buffer.
 *
 * This function implements bulk transfer.
 */
void rf22RegPut(RF22Driver *radio, uint8_t reg, RF22Length len, const RF22Buffer *buf);

/**
 * @brief Get byte(s) from hardware register(s).
 *
 * @param radio The driver pointer.
 * @param reg The hardware register address.
 * @param len The destination data length in bytes.
 * @param buf The pointer to data buffer.
 *
 * This function implements bulk transfer.
 */
void rf22RegGet(RF22Driver *radio, uint8_t reg, RF22Length len, RF22Buffer *buf);

/**
 * @}
 */
