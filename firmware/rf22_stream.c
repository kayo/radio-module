#include "ch.h"
#include "hal.h"
#include "rf22.h"
#include "rf22_stream.h"

/*
 * BaseSequentialStream Interface
 */

static size_t _rf22_write(void *instance, const uint8_t *bp, size_t n){
  RF22SequentialStream *stm = instance;
  
  if(n > 254){
    n = 254;
  }
  
  switch(rf22PacketSend(stm->opt->radio, n, bp, stm->header)){
  case RF22Success:
    return n;
  default:
    return -1;
  }
}

static size_t _rf22_read(void *instance, uint8_t *bp, size_t n){
  RF22SequentialStream *stm = instance;
  
  if(n > 254){
    n = 254;
  }

  RF22Length bs = n;
  
  switch(rf22PacketReceiveTimeout(stm->opt->radio, &bs, bp, stm->header, stm->opt->timeout)){
  case RF22Success:
    return bs;
  default:
    return -1;
  }
}

static msg_t _rf22_put(void *instance, uint8_t b){
  return 1 == _rf22_write(instance, &b, 1) ? Q_OK : Q_RESET;
}

static msg_t _rf22_get(void *instance){
  uint8_t b;
  return 1 == _rf22_read(instance, &b, 1) ? (msg_t)b : Q_RESET;
}

static const struct BaseSequentialStreamVMT _rf22_vmt = {
  /* BaseSequentialStream Interface */
  /*.write = */_rf22_write,
  /*.read = */_rf22_read,
  /*.put = */_rf22_put,
  /*.get = */_rf22_get,
};

void rf22SequentialStreamOpen(RF22SequentialStream *stm, RF22Header header, const RF22ConnectionOptions *opt){
  chDbgCheck(stm != NULL && opt != NULL, "rf22SequentialStreamOpen");
  
  stm->vmt = &_rf22_vmt;
  stm->opt = opt;
  stm->header = header;
}
