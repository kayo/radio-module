extern SerialUSBDriver SDU1;

void usbLocalInit(void);
void usbLocalStart(void);
void usbLocalStop(void);
bool_t usbLocalState(void);
