#include "rf22.h"
extern RF22Driver RF22D1;

void rf22LocalInit(void);
void rf22LocalStart(void);
void rf22LocalStop(void);
void rf22LocalIntr(EXTDriver *extp, expchannel_t channel);
void rf22RegisterWrite(uint8_t reg, uint8_t len, uint8_t *buf);
void rf22RegisterRead(uint8_t reg, uint8_t len, uint8_t *buf);
bool_t rf22LocalPing(RF22Header addr, uint8_t len, systime_t *lat);
