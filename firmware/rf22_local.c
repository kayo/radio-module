#include "ch.h"
#include "hal.h"

#include <string.h>
#include "rf22_local.h"

#ifndef RF22_LOCAL_THREAD
#define RF22_LOCAL_THREAD TRUE
#endif

/*
 * High speed SPI configuration (9MHz, CPHA=0, CPOL=0, MSb first).
 */
static const SPIConfig spicfg = {
  NULL,
  RF22_NSS_PORT,
  RF22_NSS_PAD,
  RF22_SPI_CFG
};

static const RF22Config rf22cfg = {
  &RF22_SPI,
  &spicfg,
  
  {
    RF22_SDN_PORT,
    RF22_SDN_PAD,
  },
  
  {
    {
      TXLED_PORT,
      TXLED_PAD,
      RF22LedOnLo,
    },
    {
      RXLED_PORT,
      RXLED_PAD,
      RF22LedOnLo,
    },
  },
  
  RF22_MHZ(433), // 433
  1,
  0,
  0,
  
#if RF22_USE_PREDEF_MODEM_CONFIG
  //RF22Fsk2Kbps5KHz,
  //RF22Fsk2p4Kbps36KHz,
  //RF22Gfsk2p4Kbps36KHz,
  //RF22Gfsk4p8Kbps45KHz,
  RF22Gfsk100Kbps50KHz,
  //RF22Gfsk125Kbps125KHz,
  //RF22Ook9p6Kbps335KHz,
#else
  { /* Modulation settings */
    RF22ModulationGFSK,
    RF22DataSourceFIFO,
    RF22DataClockNone,
    
    100000, /* Desired data rate */
    FALSE, /* No data inversion */

    FALSE,  /* Use XOR whitening */
    {
      FALSE, /* Disable manchester */
      FALSE,
      FALSE,
    },
    
    .fsk = {
      TRUE,
      RF22AfcPullInRangeDefault, // RF22_KHZ(50)
      50000
    },
  },
#endif
  
  {
    TRUE,  /* Use hardware tx packet handling */
    TRUE,  /* Use hardware rx packet handling */
    RF22BitsOrderMSB,
    { /* CRC options */
      RF22CrcCCIT,
      RF22CrcDataOnly,
    },
    { /* preamble detection */
      64, /* length */
      20, /* threshold */
      FALSE,
    },
    { /* sync word */
      2,
      0xd42d,
      FALSE,
    },
    { /* headers */
      4,
      {
        MAC_ADDR,
        MAC_MASK,
        FALSE,
      },
      {
        MAC_ADDR
      },
    },
    RF22DataLengthVariable,
  },
  
  { /* FIFO control */
    0, /* tx almost full */
    20, /* tx almost empty */
    44, /* rx almost full */
  },
  
  {
    { /* GPIO 0 */
      0,
      FALSE,
      RF22GpioTxStateOutput,
    },
    { /* GPIO 1 */
      0,
      FALSE,
      RF22GpioRxStateOutput,
    },
    { /* GPIO 2 */
      0,
      FALSE,
      RF22GpioVDD,
    },
  },
  
  5, /* max retry count for ack transfer */
};

RF22Driver RF22D1;

void rf22LocalIntr(EXTDriver *extp, expchannel_t channel){
  (void)extp;
  (void)channel;
  rf22SendEventFromIsr(&RF22D1);
}

const EXTChannelConfig rf22_irq = {
  EXT_CH_MODE_FALLING_EDGE | RF22_IRQ_EXT,
  rf22LocalIntr
};

#if RF22_LOCAL_THREAD
static WORKING_AREA(_rf22LocalThreadWorkArea, 1024);
static msg_t _rf22LocalThreadMain(void *arg){
  (void)arg;
  
  RF22Buffer buf[255];
  RF22Length len;
  
  for(; !chThdTerminated(chThdSelf()); ){
    len = sizeof(buf);
    if(RF22Success == rf22PacketReceive(&RF22D1, &len, buf, 0)){
      if(0 == memcmp(buf, "ping", 4)){
        memcpy(buf, "pong", 4);
        rf22PacketSend(&RF22D1, len, buf, 0);
      }
    }
  }
  
  return 0;
}
static Thread *_rf22LocalThread;
#endif

void rf22LocalInit(void){
  rf22ObjectInit(&RF22D1);
}

void rf22LocalStart(void){
  rf22Start(&RF22D1, &rf22cfg); /* start radio */
  
  /* init */
  chSysLock();
  extSetChannelModeI(&EXTD1, RF22_IRQ_PAD, &rf22_irq);
  chSysUnlock();

#if RF22_LOCAL_THREAD
  _rf22LocalThread = chThdCreateStatic(_rf22LocalThreadWorkArea, sizeof(_rf22LocalThreadWorkArea), LOWPRIO, _rf22LocalThreadMain, NULL);
#endif
}

void rf22LocalStop(void){
#if RF22_LOCAL_THREAD
  chThdTerminate(_rf22LocalThread);
#endif
  
  rf22Stop(&RF22D1); /* Stop radio. */
  
  extChannelDisable(&EXTD1, RF22_IRQ_PAD);
}

void rf22RegisterWrite(uint8_t reg, uint8_t len, uint8_t *buf){
  rf22RegPut(&RF22D1, reg, len, buf);
}

void rf22RegisterRead(uint8_t reg, uint8_t len, uint8_t *buf){
  rf22RegGet(&RF22D1, reg, len, buf);
}

bool_t rf22LocalPing(RF22Header addr, uint8_t len, systime_t *lat){
  uint8_t buf[len];
  memcpy(buf, "ping", 4);

  *lat = halGetCounterValue();
  
  RF22Status status = rf22PacketSendReceiveTimeout(&RF22D1, len, buf, addr, &len, buf, addr, S2ST(1));
  
  *lat = halGetCounterValue() - *lat;
  
  return status == RF22Success && len == sizeof(buf) && 0 == memcmp(buf, "pong", 4);
}
